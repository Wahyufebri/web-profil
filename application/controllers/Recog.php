<?php

class Recog extends CI_Controller{
		public function __construct(){
		parent:: __construct();
		$this->load->model('M_Company');
		$this->load->library('Template');
		// $this->load->model('M_Barang');
		if ($this->session->userdata('level') == "user") {
			redirect('user');
		} elseif ($this->session->userdata('level') == "") {
		    redirect('login');
		}else{
			
		}
	}
	public function admin(){
		$data['admin']=$this->M_Company->get_user('user','admin','block_admin');
		$this->template->ips('admin/user/admin',$data);
	}
	public function user(){
		$data['admin']=$this->M_Company->get_user('user','user','block_user');
		$this->template->ips('admin/user/user',$data);
	}
	function delete(){
		$page = $this->input->post('member');
		$id = $_POST['nis'];
		$this->M_Company->hapus('user',$id);
		$this->M_Company->hapus_chat('chat',$id);
		redirect("recog/$page");
	}
	function edit(){
		$page = $this->input->post('member');
		$id = $_POST['nis'];
		$data =[
			"level" => $this->input->post('edit')
		];
		$this->M_Company->block('user',$id,$data);
		redirect("recog/$page");
	}
	function unblock(){
		$page = $this->input->post('member');
		$id = $_POST['nis'];
		$data =[
			"level" => $this->input->post('unblock')
		];
		$this->M_Company->block('user',$id,$data);
		redirect("recog/$page");
	}
	function add_admin(){
		$path="./assets/images/auth/";
		$config['upload_path']  =$path;
		$config['allowed_types']="jpg|png|svg";
		$config['max_size']		=2048;
		$this->load->library('upload',$config);
		if (!$this->upload->do_upload('foto')) {
			$error=array('error' => $this->upload->display_errors());
			print_r($error);
		} else {
			$image=$this->upload->data();
			$data=[
			"username"=> $this->input->post("username"),
			"gambar"  => $image['file_name'],
			"password"=> password_hash($this->input->post("password"),PASSWORD_BCRYPT),
			"no_hp"	  => $this->input->post("no_hp"),
			"level"	  => "admin"
		];
		$this->M_Company->input('user',$data);
		redirect("recog/admin");
		}
	}
}