<?php

class User extends CI_Controller{
	public function __construct(){
		parent:: __construct();
		$this->load->model('M_Company');
		$this->load->library('Template');
		// $this->load->model('M_Barang');
	}
	public function index()
	{
		$data['active']="active";
		$data['active2']="";
		$data['active3']="";
		$data['active4']="";
		$data['isi']=$this->M_Company->get_home_2('about','home');
		$this->template->user_isi('user/isi/index',$data);
	}
	public function about(){
		$data['active']="";
		$data['active2']="active";
		$data['active3']="";
		$data['active4']="";
		$data['top']=$this->M_Company->get_wh('about','top','about')->row_array();
		$data['center']=$this->M_Company->get_wh('about','center','about')->result_array();
		$this->template->user_isi('user/isi/about',$data);
	}
	public function tim(){
		$data['active']="";
		$data['active2']="";
		$data['active3']="active";
		$data['active4']="";
		$data['tim']=$this->M_Company->ambil('team');
		$this->template->user_isi('user/isi/tim',$data);
	}
	public function project(){
		$data['active']="";
		$data['active2']="";
		$data['active3']="";
		$data['active4']="active";
		$offset = $this->uri->segment(3);
		$config['total_rows'] =$this->M_Company->hitung('project');
		$config['base_url']   =base_url()."index.php/user/project/";
		$config['per_page']   = 6;


		// style 
				$config['first_link']       = 'First';
                $config['last_link']        = 'Last';
                $config['next_link']        = 'Next';
                $config['prev_link']        = 'Prev';
                $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
                $config['full_tag_close']   = '</ul></nav></div>';
                $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
                $config['num_tag_close']    = '</span></li>';
                $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
                $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
                $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
                $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['prev_tagl_close']  = '</span>Next</li>';
                $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
                $config['first_tagl_close'] = '</span></li>';
                $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
                $config['last_tagl_close']  = '</span></li>';


        $this->pagination->initialize($config);
        $data['halaman']=$this->pagination->create_links();
        $data['offset'] =$offset;
		$data['content']=$this->M_Company->project($config['per_page'],$offset);
		$this->template->user_isi('user/isi/project',$data);
	}
	function chat($id){
		$pesan = $this->input->post('pesan');
		$data = [
			'id_user' => $id,
			'pesan'   => $pesan
		];
		$this->M_Company->input('chat',$data);
		echo($pesan);
	}
	function coment($id){
		$pesan = $this->M_Company->chat('chat',$id);
		$user  = $this->M_Company->get_id('user',$id);
		$tampil ="";
		 foreach ($pesan as $isi) {
		 	if ($isi['id_admin'] > 0) {
		 		$tampil .='  <div class="direct-chat-msg doted-border">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name text-dark float-right">Thing Service</span>
                              </div>
                              <div class="direct-chat-text_a">'.
                                $isi['pesan']
                                .'</div>
                                <div class="clearfix"></div>
                              <div class="direct-chat-info clearfix">
                                          <span class="direct-chat-timestamp pull-right text-dark">'.substr($isi['time'],10,11).'PM</span>
                                        </div>
                              <div class="direct-chat-info clearfix">
                              </div>
                              <!-- /.direct-chat-text -->
                            </div>';
		 	} else {
			$tampil .='  <div class="direct-chat-msg doted-border">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name text-dark">'.$user['username'].'</span>
                              </div>
                              <div class="direct-chat-text">'.
                                $isi['pesan']
                                .'</div>
                                <div class="clearfix"></div>
                              <div class="direct-chat-info clearfix">
                                          <span class="direct-chat-timestamp pull-left text-dark">'.substr($isi['time'],10,11).'PM</span>
                                        </div>
                              <div class="direct-chat-info clearfix">
                              </div>
                              <!-- /.direct-chat-text -->
                            </div>';

		 }
		echo $tampil;
	}
}
function trash($id){
	$this->M_Company->trash($id);
	echo "Success";
}
function search(){
		$keyword = $this->input->post('isi');
		$result = $this->M_Company->search($keyword);
		$output ='<ul class="list-unstyled">';
		if ($result->num_rows() > 0) {
			foreach ($result->result_array() as $row ) {
				$output .= '<li class="media">
						    <img src="'.base_url().'assets/images/tim/member/'.$row["gambar"].'" class="mr-3 gam" alt="..." >
						    <div class="media-body">
						      <h5 class="mt-0 mb-1 text-bold" id="nama">'.$row["nama"] .'</h5>
						      '.$row['posisi'].'
						    </div>
						  </li>'; 
			}
		}else{
			$output .='<li class="list-unstyled">Data not found</li>';
		}
		$output .='</ul>';
		echo $output;
	}
	function cari(){
		$data['active']="active";
		$data['active2']="";
		$data['active3']="";
		$data['active4']="";
		$keyword = $this->input->post('key');
		$data['keyword']=$keyword;
		$data['content'] = $this->M_Company->search($keyword)->result_array();
		$data['sum']	 = $this->M_Company->search($keyword)->num_rows();
		$this->template->user_isi("user/search",$data);
	}
	function detail(){
		$id = $this->input->post('id');
		$data = $this->M_Company->get_id('team',$id);
		$tampil ='<img src="'.base_url().'assets/images/tim/member/'.$data['gambar'].'" >
			      <h2>'.$data['nama'].'</h2>
			      <span>'.$data['devisi'].'</span><br>
			      <span class="posisi">'.$data['posisi'].'</span>
			      <p>'.$data['biodata'].'</p>
			       <div style="margin: 24px 0;">
				    <a href="#"><i class="fa fa-dribbble"></i></a> 
				    <a href="#"><i class="fa fa-twitter"></i></a>  
				    <a href="#"><i class="fa fa-linkedin"></i></a>  
				    <a href="#"><i class="fa fa-facebook"></i></a> 
				  </div>
			      <button class="out btn btn-primary xpire">Close</button>  ';
		echo $tampil;
	}
}
  
