<?php


class Project extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_Company');
		$this->load->library('Template');
		if ($this->session->userdata('level') == "user") {
			redirect('user');
		} elseif ($this->session->userdata('level') == "") {
		    redirect('login');
		}else{
			
		}
	}
	function index(){
		$data['project'] = $this->M_Company->ambil('project');
		$this->template->ips('admin/project/index',$data);
	}
	 function add_project()
	{
		$data['error']="";
		$this->form_validation->set_rules("nama","Title","required");
		if ($this->form_validation->run() ==  FALSE) {
				$this->template->ips('admin/project/add_project',$data);	
		} else {
	        $config['upload_path']='./assets/images/tim/member';
	        $config['allowed_types']='jpg|png|jpeg|gif|svg';
	        $config['max_size']='20480';

			$this->load->library('upload',$config); 
        if( !$this->upload->do_upload('foto'))	{
            $error = array('error' => $this->upload->display_errors());
           $this->template->ips('admin/project/add_project',$error);	

			} else {
				$gambar=$this->upload->data();
				$data=[
				    "nama" => $this->input->post('nama',true),
                    "desc"=> $this->input->post('isi',true),
                    "gambar"=> $gambar['file_name']
				];
			$this->M_Company->input('project',$data);
			$this->session->set_flashdata('flash','Added');
			redirect("project");
			}
		}
	}
		function delete($id){
		$this->M_Company->hapus('project',$id);
		redirect("Project");
	}
		function edit($id){
		$data['isi']=$this->M_Company->get_id('project',$id);
		$data['error']="";
		$data['error']="";
		$this->form_validation->set_rules("nama","Title","required");
		if ($this->form_validation->run() ==  FALSE) {
			$this->template->ips('admin/project/edit',$data);	
		} else {
		 if($_FILES ['foto']['name'] != ''){
 			$path='./assets/images/tim/member/';
	        $config['upload_path']=$path;
	        $config['allowed_types']='jpg|png|jpeg|gif|svg';
	        $config['max_size']='20480';

			$this->load->library('upload',$config); 
        if( !$this->upload->do_upload('foto'))	{
            $error = array('error' => $this->upload->display_errors());
        		  $this->template->ips('admin/project/edit',$error,$data);

			} else {
				$gambar=$this->upload->data();
				$id=$this->input->post('id');
				$old=$this->input->post('old');
				$data=[
				    "nama" => $this->input->post('nama',true),
                    "desc"=> $this->input->post('isi',true),
                    "gambar"=> $gambar['file_name']
				];
				unlink($path.$old);
			$this->M_Company->perbarui('project',$data,$id);
			$this->session->set_flashdata('flash','Added');
			redirect("project");
			}
		
	} else {
		$id=$this->input->post('id');
			$data=[
				     "nama" => $this->input->post('nama',true),
                    "desc"=> $this->input->post('isi',true),
				];
			$this->M_Company->perbarui('project',$data,$id);
			$this->session->set_flashdata('flash','Added');
			redirect("project");
	}
}


}
function view(){
	$id = $this->input->post('id');
	$data = $this->M_Company->get_id('project',$id);
	$tampil="";
	$tampil .='<div class="mx-auto width-auto " style="width:20%;"><img src="'.base_url().'assets/images/tim/member/'.$data['gambar'].'" width="155px" class="rounded"></div>
		<div class="container">
		<div class="desc mx-auto text-center">
		<p>Devisi : <small>'.$data['nama'].'</small></p>
		<p>Desc : <small>'.$data['desc'].'</small></p>
		</div>
		</div>';
	echo $tampil;
}

}