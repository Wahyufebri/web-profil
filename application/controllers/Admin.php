<?php

class Admin extends CI_Controller{
		public function __construct(){
		parent:: __construct();
		$this->load->model('M_Company');
		$this->load->library('Template');
		// $this->load->model('M_Barang');
		if ($this->session->userdata('level') == "user") {
			redirect('user');
		} elseif ($this->session->userdata('level') == "") {
		    redirect('login');
		}else{
			
		}
	}
	public function index(){	
		$this->template->ips('admin/utama/index');
	}
	public function content(){
		$data['isi']=$this->M_Company->ambil('isi');
		$this->template->ips('admin/isi/index',$data);
	}
	public function add()
	{
		$data['error']="";
		$this->form_validation->set_rules("judul","Title","required");
		$this->form_validation->set_rules("isi","Content","required");
		$this->form_validation->set_rules("kategori","Kategory","required");
		if ($this->form_validation->run() ==  FALSE) {
				$this->template->ips('admin/isi/add');	
		} else {
	        $config['upload_path']='./assets/image';
	        $config['allowed_types']='jpg|png|jpeg|gif';
	        $config['max_size']='20480';

			$this->load->library('upload',$config); 
        if( !$this->upload->do_upload('foto'))	{
            $error = array('error' => $this->upload->display_errors());
            $data['katalog']=$this->M_Berita->ambil('kategori');
				$this->template->ips('news/add',$error,$data);

			} else {
				$gambar=$this->upload->data();
				$data=[
				    "judul" => $this->input->post('judul',true),
                    "isi"=> $this->input->post('isi',true),
                    "gambar"=> $gambar['file_name'],
                    "kategori"=>$this->input->post('kategori',true),
                    "link"=>$this->input->post('link',true),
                    "tgl_b"=>$this->input->post('tanggal',true)
				];
			$this->M_Berita->create('berita',$data);
			$this->session->set_flashdata('flash','Added');
			redirect('News');
			}
		}
	}
	function team(){
			$data['isi'] = $this->M_Company->ambil('team');
			$this->template->ips('admin/isi/index_team',$data);	
	}
    function add_member()
	{
		$data['error']="";
		$this->form_validation->set_rules("nama","Title","required");
		$this->form_validation->set_rules("posisi","Content","required");
		$this->form_validation->set_rules("devisi","Kategory","required");
		if ($this->form_validation->run() ==  FALSE) {
				$this->template->ips('admin/isi/add_team',$data);	
		} else {
	        $config['upload_path']='./assets/images/tim/member';
	        $config['allowed_types']='jpg|png|jpeg|gif|svg';
	        $config['max_size']='20480';

			$this->load->library('upload',$config); 
        if( !$this->upload->do_upload('foto'))	{
            $error = array('error' => $this->upload->display_errors());
           $this->template->ips('admin/isi/add_team',$error);	

			} else {
				$gambar=$this->upload->data();
				$data=[
				    "nama" => $this->input->post('nama',true),
                    "posisi"=> $this->input->post('posisi',true),
                    "gambar"=> $gambar['file_name'],
                    "devisi"=>$this->input->post('devisi',true),
                    "biodata"=>$this->input->post('isi',true),
                    "sosmed"=>$this->input->post('sosmed',true),
                    "sosmed_2"=>$this->input->post('sosmed2',true),
                    "sosmed_3"=>$this->input->post('sosmed3',true),
                    "sosmed_4"=>$this->input->post('sosmed4',true),
				];
			$this->M_Company->input('team',$data);
			$this->session->set_flashdata('flash','Added');
			redirect("Admin/team");
			}
		}
	}
	function delete($quick,$id){
		$this->M_Company->hapus('team',$id);
		redirect("admin/$quick");
	}
	function edit($id){
		$data['isi']=$this->M_Company->get_id('team',$id);
		$data['error']="";
		$this->form_validation->set_rules("nama","Title","required");
		$this->form_validation->set_rules("posisi","Content","required");
		$this->form_validation->set_rules("devisi","Kategory","required");
		if ($this->form_validation->run() ==  FALSE) {
				$this->template->ips('admin/isi/edit_team',$data);	
		} else {
 if($_FILES ['foto']['name'] != ''){
 	$path='./assets/images/tim/member/';
	        $config['upload_path']=$path;
	        $config['allowed_types']='jpg|png|jpeg|gif|svg';
	        $config['max_size']='20480';

			$this->load->library('upload',$config); 
        if( !$this->upload->do_upload('foto'))	{
            $error = array('error' => $this->upload->display_errors());
           $this->template->ips('admin/isi/add_team',$error,$data);	

			} else {
				$gambar=$this->upload->data();
				$id=$this->input->post('id');
				$old=$this->input->post('old');
				$data=[
				    "nama" => $this->input->post('nama',true),
                    "posisi"=> $this->input->post('posisi',true),
                    "gambar"=> $gambar['file_name'],
                    "devisi"=>$this->input->post('devisi',true),
                    "biodata"=>$this->input->post('isi',true),
                    "sosmed"=>$this->input->post('sosmed',true),
                    "sosmed_2"=>$this->input->post('sosmed2',true),
                    "sosmed_3"=>$this->input->post('sosmed3',true),
                    "sosmed_4"=>$this->input->post('sosmed4',true),
				];
				unlink($path.$old);
			$this->M_Company->perbarui('team',$data,$id);
			$this->session->set_flashdata('flash','Added');
			redirect("Admin/team");
			}
		
	} else {
		$id=$this->input->post('id');
			$data=[
				    "nama" => $this->input->post('nama',true),
                    "posisi"=> $this->input->post('posisi',true),
                    "devisi"=>$this->input->post('devisi',true),
                    "biodata"=>$this->input->post('isi',true),
                    "sosmed"=>$this->input->post('sosmed',true),
                    "sosmed_2"=>$this->input->post('sosmed2',true),
                    "sosmed_3"=>$this->input->post('sosmed3',true),
                    "sosmed_4"=>$this->input->post('sosmed4',true),
				];
			$this->M_Company->perbarui('team',$data,$id);
			$this->session->set_flashdata('flash','Added');
			redirect("Admin/team");
	}
}
}
function view(){
	$id = $this->input->post('id');
	$data = $this->M_Company->get_id('team',$id);
	$tampil="";
	$tampil .='<div class="mx-auto width-auto " style="width:20%;"><img src="'.base_url().'assets/images/tim/member/'.$data['gambar'].'" width="155px" class="rounded-circle"></div>
		<div class="container">
		<div class="desc mx-auto text-center">
		<p>Devisi : <small>'.$data['devisi'].'</small></p>
		<p>Posisi : <small>'.$data['posisi'].'</small></p>
		<p>Biodata: '.$data['biodata'].'</p>
		<p>sosmed : '.$data['sosmed'].','.$data['sosmed_2'].','.$data['sosmed_3'].','.$data['sosmed_4'].'</p>
		</div>
		</div>';
	echo $tampil;
}
}
