<?php

class Home extends CI_Controller{
		public function __construct(){
		parent:: __construct();
		$this->load->model('M_Company');
		$this->load->library('Template');
		// $this->load->model('M_Barang');
		if ($this->session->userdata('level') == "user") {
			redirect('user');
		} elseif ($this->session->userdata('level') == "") {
		    redirect('login');
		}else{
			
		}
	}
	public function index(){
		$data['isi']=$this->M_Company->get_home('about','home');
		$data['error']="";
		$this->template->ips('admin/home/index',$data);
	}
	function add()
	{
		$data['error']="";
	    $config['upload_path']='./assets/images/about';
	    $config['allowed_types']='jpg|png|jpeg|gif|svg';
	    $config['max_size']='20480';
	    $this->load->library('upload',$config); 
        if( !$this->upload->do_upload('foto'))	{
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
			} else {
				$gambar=$this->upload->data();
				$data=[
				    "title" => $this->input->post('title',true),
                    "content"=> $this->input->post('content',true),
                    "image"=> $gambar['file_name'],
                    "bagian"=>$this->input->post('bagian',true),
                    "kode"=>$this->input->post('kode',true),
                    "sub"	=>"home"
				];
			$this->M_Company->input('about',$data);
			$this->session->set_flashdata('flash','Added');
			redirect("home");
			}
		}
		function edit(){
			$id=$this->input->post('id');
	if($_FILES ['foto']['name'] != ''){
 		$path='./assets/images/about/';
		$data['error']="";
	    $config['upload_path']=$path;
	    $config['allowed_types']='jpg|png|jpeg|gif|svg';
	    $config['max_size']='20480';
	    $this->load->library('upload',$config); 
        if( !$this->upload->do_upload('foto'))	{
            $error = array('error' => $this->upload->display_errors());
            $data['isi']=$this->M_Company->ambil('about');
          	 $this->template->ips('admin/about/index',$error,$data);	

			} else {
				$id=$this->input->post('id');
				$old=$this->input->post('old');
				$gambar=$this->upload->data();
				$data=[
				    "title" => $this->input->post('title',true),
                    "content"=> $this->input->post('content',true),
                    "image"=> $gambar['file_name'],
                    "bagian"=>$this->input->post('bagian',true),
                    "kode"=>$this->input->post('kode',true),
                    "sub"	=>"home"
				];
				unlink($path.$old);
			$this->M_Company->perbarui('about',$data,$id);
			$this->session->set_flashdata('flash','Added');
			redirect("about");
			}
		} else {
			$data=[
				   "title" => $this->input->post('title',true),
                    "content"=> $this->input->post('content',true),
                    "bagian"=>$this->input->post('bagian',true),
                    "kode"=>$this->input->post('kode',true),
                    "sub"	=>"home"
				];
			$this->M_Company->perbarui('about',$data,$id);
			$this->session->set_flashdata('flash','Added');
			redirect("about");
		}
	}
		function delete($table,$id){
		$this->M_Company->hapus('about',$id);
		redirect("about");
	}
	function view(){
	$id = $this->input->post('id');
	$data = $this->M_Company->get_id('about',$id);
	$tampil="";
	$tampil .='<div class="mx-auto width-auto " style="width:20%;"><img src="'.base_url().'assets/images/about/'.$data['image'].'" width="155px" class="rounded"></div>
		<div class="container">
		<div class="desc mx-auto text-center">
		<p>Title : <small>'.$data['title'].'</small></p>
		<p>Content : <small>'.$data['content'].'</small></p>
		<p>Bagian: '.$data['bagian'].'</p>
		</div>
		</div>';
	echo $tampil;
}
	}
