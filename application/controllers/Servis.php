<?php

class servis extends CI_Controller{
		public function __construct(){
		parent:: __construct();
		$this->load->model('M_Company');
		$this->load->library('Template');
		// $this->load->model('M_Barang');
		if ($this->session->userdata('level') == "user") {
			redirect('user');
		} elseif ($this->session->userdata('level') == "") {
		    redirect('login');
		}else{
			
		}
	}
	public function index(){
		$data['chat'] = $this->M_Company->ambil_id('chat');
		$this->template->ips('admin/servis/index.php',$data);
	}
	function balas($id){
		$data['id'] = $id;
		$data['isi'] = $this->M_Company->get_by_id('chat',$id)->result();
		$this->template->ips('admin/servis/servis.php',$data);
	}
	function balas_pesan($id_admin,$id_user){
		$pesan = $this->input->post('pesan');
		$data=[
			"id_admin" => $id_admin,
			"id_user"  => $id_user,
			"pesan"    => $pesan
		];
		$this->M_Company->input('chat',$data);
		echo ("jos");
	}
	function get_message($id_admin,$id){
		$message = $this->M_Company->get_by_id('chat',$id)->result();
		$admin   = $this->M_Company->get_id('user',$id_admin);
		$show = "";
		 foreach ($message as $key ) {
		 	if ($key->id_admin == 0) {
            $show .= '<li class="chat-item">
            			</div>
                         <div class="chat-content">
                               <h6 class="font-medium nama">'. $key->username .'</h6>
                              <div class="box bg-light-info">'. $key->pesan .'</div>
                           </div>
                       <div class="chat-time">'.substr($key->time,10,11).'PM</div>
                  </li>
                  <div class="clearfix"></div>';
             }else {
            $show .= '<li class="chat-item ">

                         <div class="chat-content">
                               <h6 class="font-medium float-right nama">'. $admin['username'].'</h6>
                               <div class="clearfix"></div>
                              <div class="box bg-light-info float-right" >'. $key->pesan .'</div>
                           </div>
                       <div class="chat-time mr-5 float-right">'.substr($key->time,10,11).'PM</div>
                       </div>
                       <div class="clearfix"></div>

                  </li>';
                 }

            }
         echo $show;

	}
	function delete($id){
		$this->M_Company->hapus_chat('chat',$id);
		redirect('servis');
	}
	function date(){
		$tgl = date("y-m-d");
		print_r($tgl);
	}
}