<?php

class M_Company extends CI_Model{
	function ambil($table){
		return $this->db->get($table)->result_array();
	}
	function input($table,$data){
		$this->db->insert($table,$data);
	}
	function hapus($table,$id){
		$this->db->where_in('id',$id);
		$this->db->delete($table);
	}
	function hapus_chat($table,$id){
		$this->db->where_in('id_user',$id);
		$this->db->delete($table);
	}
	function get_id($table,$id){
		$this->db->where('id',$id);
		return $this->db->get($table)->row_array();
	}
	function chat($table,$id){
		$this->db->where('id_user',$id);
		return $this->db->get($table)->result_array();
	}
	function ambil_id($table){
		$this->db->from($table);
		$this->db->select('count(id_user) as hasil');
		$this->db->select('user.username,chat.pesan,user.id,chat.time');
		// $this->db->select('count(id_admin = 	0 ) as total');
		$this->db->join('user','user.id = chat.id_user');
		$this->db->group_by('chat.id_user');
		$this->db->order_by('chat.time','desc');
		return $this->db->get()->result_array();
	}
	function get_by_id($table,$id){
		$this->db->from($table);
		$this->db->where('id_user',$id);
		$this->db->select('user.username,chat.pesan,user.id,chat.time,chat.id_admin');
		$this->db->join('user','user.id = chat.id_user');
		$this->db->order_by('time');
		return $this->db->get();
	}
	function perbarui($table,$data,$id){
		$this->db->where('id',$id);
        $this->db->update($table,$data);
	}	
	function block($table,$id,$data){
		$this->db->where_in('id',$id);
        $this->db->update($table,$data);
	}
	function login($username,$password){
		$cek =$this->db->where('username',$username)->limit(1)->get('user');
		if ($cek->num_rows() > 0) {
			$isi = $cek->row('password');
			if (password_verify($password,$isi)) {
				return $cek->row();
			} else {
				echo "Sorry Your is Hacker";
			}
		}
	}
	function trash($id){
		$this->db->where('id_user',$id);
		$this->db->delete('chat');
	}
	function search($keyword){
		$this->db->like('nama',$keyword);
		$this->db->or_like('devisi',$keyword);
		$this->db->or_like('posisi',$keyword);
		$this->db->limit("3");
		return $this->db->get('team');
	}
	function get_user($table,$id,$id_block){
		$this->db->where('level',$id);
		$this->db->or_where('level',$id_block);
		return $this->db->get($table)->result_array();
	}
	function get_wh($table,$where,$sub){
		$this->db->where('bagian',$where);
		$this->db->where('sub',$sub);
		return $this->db->get($table);
	}	
	function get_home($table,$where){
		$this->db->where('sub',$where);
		return $this->db->get($table)->result_array();
	}
	function get_home_2($table,$where){
		$this->db->where('sub',$where);
		$this->db->order_by('kode');
		return $this->db->get($table)->result_array();
	}
	function project($limit,$offset){
		return $this->db->get('project',$limit,$offset)->result_array();
	}
	function hitung($table){
		return $this->db->count_all($table);
	} 
}
// beetwen
// $this->db->where(‘tanggal >=’,$tgl_awal);
// $this->db->where(‘tanggal <=’,$tgl_akhir);