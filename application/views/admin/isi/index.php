    <div class="main-panel">
        <div class="content-wrapper">
            <div class="col-lg-12 stretch-card">
               <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash') ?>"></div>
                <?php if ($this->session->flashdata('flash')):?>
                <?php endif ?>
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"> News today 
                    <a href="<?= site_url("Admin/add");?>" class="btn btn-outline-primary btn-fw float-right">
                      <i class="mdi mdi-file-document"></i>Submit
                    </a>
                    </h4>
                  <br>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="datatables">
                      <thead>
                        <tr>
                          <th>
                            No
                          </th>
                          <th>
                            isi
                          </th>
                          <th>
                            gamabar
                          </th>
                          <th>
                            konten
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i=1; foreach ($isi as $b): ?>
                        <tr class="table-secondary">
                          <td>
                           <?= $i++ ?>
                          </td>
                          <td>
                           <img src="<?= base_url();?>assets/image/<?= $b->gambar ?>">
                          </td>
                          <td>
                            <?php 
                            $isi=$b->isi;
                            echo substr($isi,0,14),"..."
                            ?>
                          </td>
                          <td>
                           <?= $b->konten ?>
                          </td>
                          <td>
                          </td>
                        </tr>
                      <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>