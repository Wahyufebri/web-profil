    
<style type="text/css">
  img{
    margin-top: -20px;;
    width: 50px;
  }
</style>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Tables</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
    <div class="main-panel">
        <div class="content-wrapper">
           <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Submit News</h4>
                 <?php echo form_open_multipart('Admin/add_member');?>
                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nama</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama" />
                          </div>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Posisi</label>
                          <select name="posisi" class="form-control col-md-5">
                           <option value="Design">Design</option>
                           <option value="Tester">Tester</option>
                           <option value="Senior developer">Senior Developer</option>
                           <option value="Junior developer">Junior Developer</option>
                           <option value="Video editor">Video editor</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Devisi</label>
                          <select name="devisi" class="form-control col-md-5">
                           <option value="Design">Design</option>
                           <option value="progam">Program</option>
                           <option value="Editing">Editing</option>
                          </select>
                        </div>
                      </div>
                      

<!--        
      
       -->
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label" >Profil</label>
                          <div class="col-sm-9">
                            <input type="file" class="form-control dropify" data-height="100" name="foto"/>
                          </div>
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-md-2 mb-2 col-form-label">
                            <img src="<?= base_url()?>assets/images/tim/fb.svg" class="gm"></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="sosmed" placeholder="facebook" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label"><img src="<?= base_url()?>assets/images/tim/twitter.svg" class="gm"></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="sosmed2" placeholder="twitter" />
                          </div>
                        </div>
                      </div>

                    </div>
                                         <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label"> <img src="<?= base_url()?>assets/images/tim/instagram.svg" class="gm"></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="sosmed3"  placeholder="instagram" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label"> <img src="<?= base_url()?>assets/images/tim/google-plus.svg" class="gm"></label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="sosmed4" placeholder="google-plus" />
                          </div>
                        </div>
                      </div>

                    </div>
                      <h4 class="">Content</h4>
                     <textarea class="form-control"  id="editor" style="height: 300px;"  rows="8" name="isi"></textarea>
                    </div>
                    <div>
                      <button type="submit" class="btn btn-outline-info btn-fw float-right m-3">
                          <i class="mdi mdi-upload"></i>Upload</button>
                          <?= $error ?>
                     </div>
                  </form>
                </div>
              </div>
            </div>
