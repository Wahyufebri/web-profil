 <style type="text/css">
     .nama{
        text-transform: capitalize;
     }
 </style>
 <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
 <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Chat Option</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Send</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Chat Option</h4>
                                <div class="chat-box scrollable" style="height:475px;">
                                    <!--chat Row -->
                                    <ul class="chat-list" id="coment">
                                        <!--chat Row -->

                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="input-field m-t-0 m-b-0">
                                            <input type="hidden" name="isi" id="user" value="<?= $id ?>" >
                                            <textarea id="input" placeholder="Type and enter" class="form-control border-0"></textarea>
                                        </div><br>
                                        <div class="ml-2">
                                        <input checked type="checkbox" id="enter"/>
                                        <label>Send on enter</label>
                                      </div>
                                    </div>

                                    <div class="col-2">
                                        <a class="btn-circle btn-lg btn-cyan float-right text-white" href="javascript:void(0)" id="send"><i class="fas fa-paper-plane"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
        </div>
        
        <script type="text/javascript">
               $(document).ready(function(){
              $("#input").keypress(function(event){
                if (event.which == 13) {
                  if ($("#enter").prop("checked")) {
                    //console.log("kirim");
                    $("#send").click();
                    event.preventDefault();
                  }
                }
              })
  //    setInterval(function(){
  //     $('#comment').load('<?= site_url('/user/coment/').$this->session->userdata('id');?>')
  //   },100);
  $("#send").click(function(){
    var message = $("#input").val();
    var id =$("#user").val();
    $("#input").val("");
    $.ajax({
        method : "POST",
        url    : '<?= site_url("servis/balas_pesan/").$this->session->userdata('id')?>'+'/'+id,
        data   : {pesan:message},
        success:function(data){
            console.log(data);
        }
    })

  })
  setInterval(function(){
    var id =$("#user").val();
    $("#coment").load('<?= site_url("/servis/get_message/").$this->session->userdata("id")?>'+"/"+id)
  },1000)
})
        </script>
