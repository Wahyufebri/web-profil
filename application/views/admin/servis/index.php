 <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Customer service</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Service</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Recent Posts</h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                <!-- Comment Row -->
                                <?php foreach($chat as $c):?>
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="p-2"><img src="<?= base_url()?>assets/images/logo.png" alt="user" width="50" class="rounded-circle"></div>
                                    <div class="comment-text w-100">
                                        <h6 class="font-medium"><?= $c['username']?></h6>
                                        <span class="m-b-15 d-block"><?= $c['pesan']?></span>
                                        <div class="comment-footer">
                                            <span class="text-muted float-right"><?= $c['time']?></span> 
                                            <a href="<?= site_url("servis/balas/").$c['id'] ?>" class="btn btn-cyan btn-sm">Balas</a>
                                            <a href="<?= site_url("servis/delete/").$c['id'] ?>" class="btn btn-danger btn-sm">Delete</a>
                                        </div>
                                    </div>
                                </div>
                              <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                          <!-- accoridan part -->