  
  
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Tables</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
   <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title m-b-0">Manage Admin</h5>
                                <a data-toggle="modal" data-target="#Modal3" class="btn btn-primary float-right mb-3"><i class="mdi mdi-cube-send"></i> Add admin</a>
                            </div>
                                <div class="table-responsive">
                                   <form method="post" action="<?php echo site_url('recog/delete/') ?>" id="form-delete">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                 <th>
                                                    <label class="customcheckbox m-b-20">
                                                        <input type="checkbox" id="check-all" />
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </th>
                                                <th scope="col">Profile</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">E-mail</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody class="customtable">
                                          <?php foreach ($admin as $a) :?>
                                            <tr>
                                                <th>
                                                    <label class="customcheckbox">
                                                        <input type="checkbox" class="listCheckbox check-item" name='nis[]' value="<?= $a['id']?>" />
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </th>
                                                <td>
                                                  <img src="<?= base_url();?>assets/images/auth/<?= $a['gambar'] ?>" height="50px" width="50px;" style="border-radius: 50%;" >
                                                </td>
                                                <td><?= $a['username']?></td>
                                                <td><?= $a['email']?></td>
                                                     <?php if ($a['level'] == "admin") {?>
                                                       <td class="bg-green">Normal </td>
                                                    <?php } else {?>
                                                        <td class="bg-pink">Block</td>
                                                    <?php } ?>
                                            </tr>
                                          <?php endforeach ?>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="member" value="admin">
                                    <input type="hidden" name="edit" value="block_admin">
                                    <input type="hidden" name="unblock" value="admin">
                                       <button type="submit" class="btn btn-danger" id="btn-delete">Delete</button>
                                       <button type="submit" class="btn btn-warning text-dark"  id="btn-edit">Block</button>
                                       <button type="submit" class="btn btn-info"  id="btn-un">Un Block</button>
                                    </form>
                                </div>
                        </div>
                        <!-- MOdal -->
                                <div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Add Admin</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form method="post" action="<?= site_url("recog/add_admin")?>" enctype="multipart/form-data" id="form">
                                         <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" class="form-control" placeholder="Input title content" name="username">
                                          </div>
                                         <div class="form-group">
                                            <label>Password</label>
                                            <input type="Password" class="form-control" placeholder="Input title content" name="password">
                                          </div>
                                          <div class="form-group">
                                            <label>Contact number</label>
                                            <input type="text" min="08" class="form-control" value="+62" name="no_hp">
                                          </div>
                                          <div class="form-group">
                                            <label>Profile</label>
                                            <input type="file" name="foto" class="form-control">
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                                        <button type="submit" class="btn btn-primary" id="tombol" >Save</button>
                                      </div>
                                  </form>
                                    </div>
                                  </div>
                                </div