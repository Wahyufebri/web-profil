  
  
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Tables</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
   <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title m-b-0">Manage Admin</h5>
                            </div>
                                <div class="table-responsive">
                                     <form method="post" action="<?php echo site_url('recog/delete') ?>" id="form-delete">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                  <th>
                                                    <label class="customcheckbox m-b-20">
                                                        <input type="checkbox" id="check-all" />
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </th>
                                                <th scope="col">Profile</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">E-mail</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody class="customtable">
                                          <?php foreach ($admin as $a) :?>
                                            <tr>
                                                <th>
                                                    <label class="customcheckbox">
                                                        <input type="checkbox" class="listCheckbox check-item" name='nis[]' value="<?= $a['id']?>" />
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </th>
                                                <td>No image file</td>
                                                <td><?= $a['username']?></td>
                                                <td><?= $a['email']?></td>
                                                    <?php if ($a['level'] == "user") {?>
                                                       <td class="bg-green">Normal </td>
                                                    <?php } else {?>
                                                        <td class="bg-pink">Block</td>
                                                    <?php } ?>
                                            </tr>
                                          <?php endforeach ?>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="member" value="user">
                                    <input type="hidden" name="edit" value="block_user">
                                    <input type="hidden" name="unblock" value="user">
                                       <button type="submit" class="btn btn-danger" id="btn-delete">Delete</button>
                                       <button type="submit" class="btn btn-warning text-dark"  id="btn-edit">Block</button>
                                       <button type="submit" class="btn btn-info"  id="btn-un">Un Block</button>
                                    </form>
                                </div>
                        </div>
