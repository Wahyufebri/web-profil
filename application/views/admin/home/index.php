
  
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Tables</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Team member</h5>
                                <a data-toggle="modal" data-target="#Modal1" data-controler="home"  class="btn btn-primary float-right mb-3 text-white" id="plus" ><i class="mdi mdi-cube-send"></i> Add Content</a>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                           <tr>
                                            <th>No</th>
                                            <th>Sub Title</th>
                                            <th>Image</th>
                                            <th>Content</th>
                                            <th>Bagian</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                                <?php $i=1; foreach ($isi as $b): ?> 
                                            <tr id="dalam" data-id="<?= $b['id']?>" data-title=" <?= $b['title']?>" data-content="<?= $b['content']?>" data-bagian="<?= $b['bagian']?>" data-table="project" data-image="<?= $b['image']?>" data-controler="home" >
                                              <td>
                                                <?= $i++ ?> 
                                              </td>
                                              <td> <?= $b['title']?> </td>
                                              <td>
                                               <img src="<?= base_url();?>assets/images/about/<?= $b['image']?>" height="50px" width="50px;" style="border-radius: 50%;" >
                                              </td>
                                               <td> <?= $b['content']?> </td>
                                                <td> <?= $b['bagian']?> </td>
                                          <?php endforeach ?> 
                                          </tbody>
                                          <?= $error?>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
                                <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form method="post" action="" enctype="multipart/form-data" id="form">
                                         <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" id="sub" placeholder="Input title content" name="title" id="judul">
                                          </div>
                                            <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Content</label>
                                            <textarea class="form-control" id="isi" rows="3" name="content"></textarea>
                                          </div>
                                            <div class="form-group">
                                            <label for="exampleFormControlSelect1">Display</label>
                                            <select class="form-control" id="exampleFormControlSelect1" name="bagian">
                                              <option value="top" id="top">Top</option>
                                              <option value="center" id="center">Center</option>
                                              <option value="bottom" id="bottom">Bottom</option>
                                            </select>
                                          </div>
                                            <div class="form-group">
                                            <label for="exampleFormControlSelect1">Kode</label>
                                            <select class="form-control" id="exampleFormControlSelect1" name="kode">
                                              <option value="1" id="top">1</option>
                                              <option value="2" id="center">2</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label for="foto">Image</label>
                                            <input type="file" class="form-control-file" id="foto"  name="foto">
                                          </div>
                                          <input type="hidden" name="id" id="id">
                                          <input type="hidden" name="old" id="img">
                                          <div id="image">
                                              
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
                                        <button type="submit" class="btn btn-primary" id="tombol" >Save</button>
                                      </div>
                                  </form>
                                    </div>
                                  </div>
                                </div>




                                                    <div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body" id="content">
                                               
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <style type="text/css">
                                    .desc{
                                     
                                    }
                                </style>
