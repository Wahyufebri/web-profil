 
 <br><!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
                      <div id='cntnr'>
    <ul id='items' class="list-unstyled">
      <li id="edit" ><a href="" class="btn text-dark" data-toggle="modal" data-target="#Modal1" > <i class="mdi mdi-camera-iris"></i> Edit</a></li>
      <li id="view" data-toggle="modal" data-target="#Modal3"><a class="btn text-dark"><i class="mdi mdi-magnify-plus"></i> View</a></li>
      <li id="delete"><a href="" class="btn text-dark"><i class="mdi mdi-server-remove"></i> Delete</a></li>  
    </ul>
  </div>
            <footer class="footer text-center">
                All Rights Reserved by Thing~Group. Designed and Developed by <a href="https://wrappixel.com">thingwebex</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?= base_url() ;?>assets/admin/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url() ;?>assets/admin/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?= base_url() ;?>assets/admin/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url() ;?>assets/admin/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url() ;?>assets/admin/dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="<?= base_url() ;?>assets/admin/assets/libs/flot/excanvas.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/flot/jquery.flot.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/flot/jquery.flot.time.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?= base_url() ;?>assets/admin/dist/js/pages/chart/chart-page-init.js"></script>
    <script src="<?= base_url() ;?>assets/admin/assets/extra-libs/DataTables/datatables.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
         $('#zero_config').DataTable();
       });
    </script>
    <script type="text/javascript">
$("#dalam ").bind("contextmenu",function(e){
  e.preventDefault();  
  // console.log(e.pageX + "," + e.pageY);
  $("#cntnr").css("left",e.pageX);
  $("#cntnr").css("top",e.pageY);  
  $("#cntnr").hide(100);   
  $("#cntnr").fadeIn(200,startFocusOutt());  
  var id =$(this).attr("data-id"); 
  var table =$(this).attr("data-table");
  var controler =$(this).attr("data-controler");
  var title = $(this).attr("data-title");
  var content = $(this).attr("data-content");
  var bagian  = $(this).attr("data-bagian");
  var image   = $(this).attr("data-image");
  var gambar  = '<img src="<?= base_url();?>assets/images/about/'+image+'" height="50px" width="50px;" style="border-radius: 50%;" >'
  // console.log(table);
  if (controler == "about") {
    $("#edit > a").attr("href","#");
        $("#edit").click(function(){
            console.log(id+title+content+bagian);
            $("#sub").val(title);
            $("#"+bagian).attr('selected', 'selected');
            $("#isi").val(content);
            $("#image").html(gambar);
            $("#tombol").html("edit");
            $("#exampleModalCenterTitle").html("Form Edit Data");
            $("#form").attr('action','<?= site_url("about/edit") ?>');
            $("#id").val(id);
            $("#image").val(image);
       })
  } else if(controler == "home"){
              $("#edit").click(function(){
            console.log(id+title+content+bagian);
            $("#sub").val(title);
            $("#"+bagian).attr('selected', 'selected');
            $("#isi").val(content);
            $("#image").html(gambar);
            $("#tombol").html("edit");
            $("#exampleModalCenterTitle").html("Form Edit Data");
            $("#form").attr('action','<?= site_url("home/edit") ?>');
            $("#id").val(id);
            $("#image").val(image);
       })
  }else{
    $("#edit > a").attr("data-toggle","");
    $("#edit > a").attr("href","<?= site_url("/");?>"+controler+"/edit/"+id);   
  }
  // $("#view > a").attr("href","<?= site_url("/");?>"+table+"/"+id);   
  $("#view").click(function(){
    $.ajax({
      method : "POST",
      url    : "<?= site_url("/");?>"+controler+"/view/"+id,
      data   :{id:id},
      success:function(data){
        $("#content").html(data);
      }
    })
  })
  $("#delete > a").attr("href","<?= site_url("/");?>"+controler+"/delete/"+table+"/"+id); 
  $(document).click(function(){
    $("#cntnr").hide();
  }) 

});
function startFocusOut(){
  $("#isi > td").on("click",function(){
  $("#cntnr").hide();        
  $(this).off("click");
  });
}
function startFocusOutt(){
  $("#dalam > td").on("click",function(){
  $("#cntnr").hide();        
  $(this).off("click");
  });
}
$("#items > li").click(function(){
$("#op").text("You have selected "+$(this).text());
});
</script>
    </script>
                 <script>
        //***********************************//
        // For select 2
        //***********************************//
        // $(".select2").select2();

        /*colorpicker*/
        // $('.demo').each(function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        // $(this).minicolors({
        //         control: $(this).attr('data-control') || 'hue',
        //         position: $(this).attr('data-position') || 'bottom left',

        //         change: function(value, opacity) {
        //             if (!value) return;
        //             if (opacity) value += ', ' + opacity;
        //             if (typeof console === 'object') {
        //                 console.log(value);
        //             }
        //         },
        //         theme: 'bootstrap'
        //     });

        // });
        /*datwpicker*/
        // jQuery('.mydatepicker').datepicker();
        // jQuery('#datepicker-autoclose').datepicker({
        //     autoclose: true,
        //     todayHighlight: true
        // });

    </script>
          <script type="text/javascript">
    $(document).ready(function(){
        $("#check-all").click(function(){
            if($(this).is(":checked"))
                $(".check-item").prop("checked",true);
            else
                $(".check-item").prop("checked",false);
        });

        $("#btn-delete").click(function(){
            var confirm =window.confirm("Ara you sure to delete this data");
            if (confirm)
                $("#form-delete").submit();
        });
        $("#btn-edit").click(function(){
          $("#form-delete").attr('action','<?= site_url("recog/edit")?>');
          $("#form-delete").submit();
        })
        $("#btn-un").click(function(){
          $("#form-delete").attr('action','<?= site_url("recog/unblock")?>');
          $("#form-delete").submit();
        })
       $('#plus').click(function(){
        var controler =$(this).attr("data-controler");
        $('#form')[0].reset();
         $("#image").html("");
         $("#tombol").html("Save");
         $("#exampleModalCenterTitle").html("Form Tambah Data");
          $("#form").attr('action','<?= site_url('/')?>'+controler+'/add');
        });
    })
    </script>



</body>

</html>

<!--    //update record to database
         $('#btn_update').on('click',function(){
            var product_code = $('#product_code_edit').val();
            var product_name = $('#product_name_edit').val();
            var price        = $('#price_edit').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('product/update')?>",
                dataType : "JSON",
                data : {product_code:product_code , product_name:product_name, price:price},
                success: function(data){
                    $('[name="product_code_edit"]').val("");
                    $('[name="product_name_edit"]').val("");
                    $('[name="price_edit"]').val("");
                    $('#Modal_Edit').modal('hide');
                    show_product();
                }
            });
            return false;
        }); -->