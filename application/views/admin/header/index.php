<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ;?>assets/images/logo.png"">
    <title>Thing_Group</title>
    <!-- Custom CSS -->
    <link href="<?= base_url() ;?>assets/admin/assets/libs/flot/css/float-chart.css" rel="stylesheet">
     <link href="<?= base_url() ;?>assets/admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="<?= base_url() ;?>assets/admin/dist/css/style.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <style type="text/css">
        <style type="text/css">
  #items{
  list-style:none;
  margin:0px;
  margin-top:4px;
  padding-left:10px;
  padding-right:10px;
  padding-bottom:3px;
  font-size:17px;
  color: #333333;
  
}
hr { width: 85%; 
  background-color:#E4E4E4;
  border-color:#E4E4E4;
    color:#E4E4E4;
}
#cntnr{
  display:none;
  overflow: auto;
  position:fixed;
  border:1px solid #B2B2B2;
  width:150px;      background:#F9F9F9;
  box-shadow: 3px 3px 2px #E9E9E9;
  border-radius:4px;
}

li{
  
  padding: 3px;
  padding-left:10px;
}


#items :hover{
   /*color: white;*/
  background:#dfe1e4;
  border-radius:2px;
} 
 #preloader {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background-color: #fff;
}
#preloader .loading {
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%,-50%);
  font: 14px arial;
}
.bg-pink{
  background-color: pink;
}
.bg-green{
  background-color: #8dd68d;
}
</style>
    </style>
    <link href="<?= base_url() ;?>assets/admin/dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<?= $nav ?>
<?= $content ?>
<?= $footer ?>