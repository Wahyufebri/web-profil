<style type="text/css">
  .foot-about img{
    width: 93px;
    vertical-align: baseline;
  }
  .foot-about h2{
    font-size: 30px;
    font-family:'PT Serif', serif;
  }
  footer ul li a{
    color: #fff;
  }
  .foot-contact ul li i{
    margin-right: 3px;
  }
</style>
<div class="content">
</div>
<footer class="site-footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="foot-about">
                        <h2>Thing_group</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
                            <ul class="d-flex flex-wrap align-items-center">
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>

                    </div>
                </div>
                <!-- 2 -->
                <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <h2>Useful Links</h2>

                        <ul>
                            <li><a href="#">Privacy Polticy</a></li>
                            <li><a href="#">Ali baba group</a></li>
                            <li><a href="#">Amazon</a></li>
                            <li><a href="#">Tokopedia</a></li>
                            <li><a href="#">Google</a></li>
                            <li><a href="#">Microsoft</a></li>
                            <li><a href="#">Softbank</a></li>
                        </ul>
                    </div>
                    <!-- 3 -->
                      <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <div class="foot-latest-news">
                            <h2>Latest News</h2>

                            <ul>
                                <li>
                                    <h3><a href="#">Penerimaan Anggota baru</a></h3>
                                    <div class="posted-date">January 3, 2019</div>
                                </li>

                                <li>
                                    <h3><a href="#">Kuis project </a></h3>
                                    <div class="posted-date">December 12, 2019</div>
                                </li>

                                <li>
                                    <h3><a href="#">Sales new project</a></h3>
                                    <div class="posted-date">March 5, 2019</div>
                                </li>
                            </ul>
                        </div><!-- .foot-latest-news -->
                    </div>

                    <!-- 4 -->

                    <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                        <div class="foot-contact">
                            <h2>Contact</h2>

                            <ul>
                                <li><i class="fa fa-phone"></i><span>+45 677 8993000 223</span></li>
                                <li><i class="fa fa-envelope"></i><span>Thing group@thing.com</span></li>
                                <li><i class="fa fa-map-marker"></i><span>Ngentak,Baturetno-Bantul,Banguntapan km 13</span></li>
                            </ul>
                        </div><!-- .foot-contact -->

                        <div class="subscribe-form">
                            <form class="d-flex flex-wrap align-items-center">
                                <input type="email" placeholder="Your email">
                                <input type="submit" value="send">
                            </form><!-- .flex -->
                        </div><!-- .search-widget -->
                    </div><!-- .col -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div><!-- .footer-widgets -->

        <div class="footer-bar">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p class="m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> <a href="https://thing.com" target="_blank">Thing_group.com</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
            </div>
        </div>
    </div>
</footer>

<?php if ($this->session->userdata('id') > 0) {?>
  <div class="container text-center">
  <div class="row">
        <div class="tombol">
        <a  id="addClass"><span class="glyphicon glyphicon-comment"></span> Customer service </a>
        </div>
  </div>
</div>
<?php } else {?>
<div class="container text-center">
  <div class="row">
        <div class="tombol">
        <a href="<?= site_url("login")?>" class="text-decoration-none" style="    color: #ff6701;"><span class="glyphicon glyphicon-comment"></span> Customer service </a>
        </div>
  </div>
</div>
<?php } ?>

<div class="popup-box chat-popup" id="qnimate">
      <div class="popup-head">
        <div class="popup-head-left pull-left"><img src="<?= base_url()?>assets/images/logo.png" alt="iamgurdeeposahan" height="44px;">Thing~Service</div>
            <div class="popup-head-right pull-right">
            <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="glyphicon glyphicon-off"></i></button>
                              </div>
                </div>
              <div class="popup-messages">
              <div class="direct-chat-messages" id="comment">
                 <!--  <div class="chat-box-single-line">
                        <abbr class="timestamp">October 8th, 2015</abbr>
                  </div> -->
                  <!-- Message. Default to the left -->

                            <!-- /.direct-chat-msg -->
                 </div> 
              </div>
              <div class="popup-messages-footer">
              <textarea  id="pesan" placeholder="Type a message..." rows="10" cols="40" name="message" ></textarea>
                <br>
                <div class="ml-2">
                <input checked type="checkbox" id="enter"/>
                <label>Send on enter</label>
              </div>
              <div class="btn-footer">
              <a id="trash" class="bg_none text-center"><i class="glyphicon glyphicon-trash"></i> </a>
                    <button class="bg_none"><i class="glyphicon glyphicon-paperclip"></i> </button>
              <button class="bg_none pull-right" id="send"><i class="glyphicon glyphicon-send"></i> </button>
              </div>
              </div>
            </div>
            <script src="<?= base_url() ;?>assets/admin/assets/libs/jquery/dist/jquery.min.js"></script>
            <script type="text/javascript">
               $(document).ready(function(){
              $("#pesan").keypress(function(event){
                if (event.which == 13) {
                  if ($("#enter").prop("checked")) {
                    console.log("kirim");
                    $("#send").click();
                    event.preventDefault();
                  }
                }
              })
              $("#send").click(function(){
                var message = $("#pesan").val();
                $("#pesan").val("");
                console.log(message);
                $.ajax({
                  method:"POST",
                  url:"<?php echo site_url('user/chat/').$this->session->userdata('id');?>",
                  data  : {pesan : message},
                  success:function(data){
                    console.log(data);
                  }
                })
                $("#comment").scrollTop($("#comment").prop("scrollHeight"));
              })
                 setInterval(function(){
                  $('#comment').load('<?= site_url('/user/coment/').$this->session->userdata('id');?>')
                },1000);
              });
            </script>
            <script type="text/javascript" src="<?= base_url();?>assets/js/jquery.min.js"></script>
            <!-- <script type="text/javascript" src="<?= base_url();?>assets/ipul.js"></script> -->
            <script type="text/javascript" src="<?= base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
            <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
            <script type="text/javascript" src="<?= base_url();?>assets/ipul_2.js"></script>
            <script type="text/javascript">
                $(function(){
            $("#addClass").click(function () {
                      $('#qnimate').addClass('popup-box-on');
                        });
                      
                        $("#removeClass").click(function () {
                      $('#qnimate').removeClass('popup-box-on');
                        });
              })
            </script>
            <script type="text/javascript">
              var table ="isi";
              $("#trash").on('click',function(){
                $.ajax({
                  method:"POST",
                  url   :"<?= site_url("user/trash/").$this->session->userdata('id')?>",
                  data:{isi:table},
                  success:function(data){
                    alert(data);
                  }
                })
              })
            </script>
            <script type="text/javascript">
              $(document).ready(function(){
                $(".ipul").hover(function(){
                $(this).attr('class','mb-2 col-md-12 ipul shadow bg-white rounded');
              },function(){
                $(this).attr('class','col-md-12 ipul bg-white rounded');
              });
            });
            </script>
                <script type="text/javascript">
                $(document).ready(function(){
                  $('#auto').keyup(function(){
                    var isi= $(this).val();
                    if (isi != '') {
                        $.ajax({
                          url:"<?php echo site_url('user/search');?>",
                          method:"POST",
                          data:{isi:isi},
                          success:function(data){
                            // console.log(data);
                             $('#kategorilist').fadeIn(100);
                             $('#kategorilist').html(data);
                          }
                        });
                    } else{
                      $("#kategorilist").fadeOut();
                    }
                  });
                    $(document).on('click','li',function(){
                            $('#auto').val($("#nama",this).text());
                            $('#kategorilist').fadeOut();
                        });
                });
    </script>
    <script type="text/javascript">
      //appends an "active" class to .popup and .popup-content when the "Open" button is clicked
      function tampil(id){
        $(".popup-overlay, .popup-content").addClass("active",250);
            $.ajax({
              method : "POST",
              url    : "<?= site_url("user/detail/");?>",
              data   :{id:id},
              success:function(data){
                $("#convert").html(data);
              }
            })
          };

      //removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
      $(".out, .popup-overlay").on("click", function(){
        $(".popup-overlay, .popup-content").removeClass("active");
      });

    </script>
    <script type="text/javascript">
      $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
var next = $(this).next();
if (!next.length) {
next = $(this).siblings(':first');
}
next.children(':first-child').clone().appendTo($(this));

for (var i=0;i<3;i++) { next=next.next(); if (!next.length) { next=$(this).siblings(':first'); }
  next.children(':first-child').clone().appendTo($(this)); } });
    </script>

</body>
</html>

