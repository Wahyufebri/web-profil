
<style type="text/css">
    
.cari form{
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    transition: all 1s;
    width: 238px;
    cursor: pointer;
    height: 50px;
    background: white;
    box-sizing: border-box;
    border-radius: 25px;
    border: 4px solid white;
    padding: 5px;
    margin-top: 16px;
}

.cari input{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;;
    height: 31.5px;
    line-height: 30px;
    outline: 0;
    border: 1px solid #4aa1ef;
     display: block;
    font-size: 1em;
    border-radius: 20px;
    padding: 0 20px;
}

.cari .fa{
box-sizing: border-box;
padding: 9px;
width: 31.5px;
height: 31.5px;
position: absolute;
top: 0;
right: 0;
border: 1px solid #4aa1ef;
border-radius: 50%;
color: #07051a;
text-align: center;
font-size: 0.9em;
transition: all 0.5s;
}

/*.cari form:hover{
    width: 260px;
    cursor: pointer;
}*/

/*.cari form:hover input{
    display: block;
}*/

.cari form:hover .fa{
    background: #4aa1ef;
    color: white;
}
.cari .join{
    background-color: #4aa1ef;
    color: #fff;
    border-radius: 20px;
    /*height: 31.5px;*/
    width: 71px;
    margin-right: -100px;
    margin-top: -30%;  
}

.cari .out{
    border-radius: 20px;
    width: 70px;
    margin-right: -200px;
    margin-top:-64px;
}
.cari .out img{
    width: 25px;

}
#kategorilist{
    z-index: 999;
    display: none;
    margin-top: 54px;
    position: fixed;
    width: 300px;
    float: right;
    background: #fff;
    margin-left: 70%;
}
#kategorilist li{
/*    border-bottom: 1px solid #d0cccc;*/
    padding-top: 15px;
    cursor: pointer;
    padding-left: 54px;
}
#kategorilist li:hover{
    background-color: #bbbbbb63
}
#kategorilist i{

}

.gam{
    width: 25%;
}

</style>
<header class="main_h  sticky"">
<div class="container">
    <div class="row">
        <a class="logo" href="<?= base_url()?>"><img src="<?= base_url();?>assets/images/logo.png" width="50"><p class="float-right">Thing</p></a>

        <div class="mobile-toggle">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <nav>
            <ul>
                <li><a href="<?= site_url('user');?>" class="<?= $active ?>"><i class="fas fa-home"></i></a></li>
                <li><a href="<?= site_url('user/about');?>" class="text-decoration-none <?= $active2 ?>">About</a></li>
                <li><a href="<?= site_url('user/tim');?>"class="text-decoration-none <?= $active3 ?>" >Tim Work</a></li>
                <li><a href="<?= site_url('user/project');?>" class="text-decoration-none <?= $active4 ?>">Portofolio</a></li>
            </ul>
        </nav>

    </div> 
    <div class="cari">
        <?= form_open("user/cari")?>
        <input type="search" id="auto" name="key" autocomplete="off">
        <button type="submit"><i class="fa fa-search"></i></button></form>
        <?php if ($this->session->userdata('id') == TRUE) { ?>
            <a href="<?= site_url("Login/log_out");?>" class="btn float-right out "><i class="fas fa-sign-out-alt"></i></a>
        <?php }else { ?>
            <a href="<?= site_url("Login");?>" class="btn btn-light float-right join">Join</a>
        <?php }?>
       
    </div>

</div>

</header>
   <div id="kategorilist" class="shadow-lg rounded-lg"></div>