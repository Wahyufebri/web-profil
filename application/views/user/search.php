<style type="text/css">

.popup-overlay{
  /*Hides pop-up when there is no "active" class*/
  visibility:hidden;
  margin-top: 7%;
  position:fixed;
  background:#ffffff;
  border:3px solid #4aa1ef;
  width:50%;
  height:auto;
  left:25%; 

}
.popup-overlay.active{
  /*displays pop-up when "active" class is present*/
  visibility:visible;
  text-align:center;
}

.popup-content {
  /*Hides pop-up content when there is no "active" class */
 visibility:hidden;
}

.popup-content img{
  width: 20%;
  height: 30%;
}
.popup-content h2{
  font-family: 'Margarine', cursive;
}
.popup-content.active {
  /*Shows pop-up content when "active" class is present */
  visibility:visible;
}
.hasil{
  display: none;
}
.dlm{
  background-color: pink;
  height: 100%;
}
.xpire{
  margin-top: 10%;
}
.popup-content a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

.popup-content button:hover, a:hover {
  opacity: 0.7;
  cursor: pointer;
}
.posisi {
  font-size: 30px;
  color: #828282;
  font-family:'PT Serif', serif;
}
.title-search{
  margin-top: 10%;
  text-align: center;
}
.title-search small{
  font-size: 13px;
}
</style>
<div class="none">
 <div class="container gallery-container dadi">

    <h5 class="title-search"> All result <small><?= $sum ?></small>  From   <small><?= $keyword ?></small></h5>

<!--     <p class="page-description text-center">
    	<?php
		// Mengambil waktu awal proses
		$mtime = microtime();
		$mtime = explode (" ", $mtime);
		$mtime = $mtime[1] + $mtime[0];
		$tstart = $mtime;
		?>
		<?php
		// mengambil waktu selesai
		$mtime = microtime();
		$mtime = explode (" ", $mtime);
		$mtime = $mtime[1] + $mtime[0];
		// Store end time in a variable
		$tend = $mtime;
		// Calculate Difference
		$totaltime = ($tend - $tstart);
		// Output the result
		printf ("Waktu menampilkan halaman %f detik.", $totaltime);
		?></br>
		All result :<?= $sum ?>
    </p>
     -->
    <div class="tz-gallery">

        <div class="row">
        <?php foreach($content as $key ):?>
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <a class="lightbox " onclick="tampil('<?= $key['id']?>')" >
                       <img src="<?= base_url() ;?>assets/images/tim/member/<?= $key['gambar'] ?>" alt="Park" height=" 200px">
                    </a>
                    <div class="caption">
                        <h3><?= $key['nama']?></h3>
                        <p><?= $key['posisi']?></p>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
        </div>

    </div>

</div>
</div>
<!--Creates the popup body-->
<div class="popup-overlay  rounded-lg shadow-lg ">
  <!--Creates the popup content-->
   <div class="popup-content " id="convert">
    
      </div>
</div>
<!--Content shown when popup is not displayed-->