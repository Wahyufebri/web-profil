<style type="text/css">
.ipul{
    cursor: pointer;
}
.ipul p{
    font-size: 13px;
}
.jin{
    font-size: 14px;
}
.ipul img{
    height: 180px;
    width: 180px;
    float: left;
     margin: 2%;"
}
.ipul .kanan{
    height: 180px;
    width: 180px;
    float: right;
     margin: 2%;"
}
    .about .options img {
    width: 100px;
}
.name{
   background: url('<?= base_url() ?>assets/info.jpg') no-repeat center; 
   position: relative;
    z-index: 99;
    padding-top: 100px;
    padding-bottom: 72px;
    background-size: cover !important;
}
.name::after{
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        background-color: rgba(21, 21, 21, 0.71);
    }
.name h1{
    position: relative;
    padding-bottom: 24px;
    font-size: 20px;
    font-weight: 600;
    color: #fff;
}
.name h1::before{
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 64px;
    height: 4px;
    border-radius: 2px;
    background: #ff5a00;
}
.title-content{
    position: relative;
    padding-bottom: 24px;
    font-size: 20px;
    margin-top: 5%;
    font-weight: 600;
}
.title-content::before{
        content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 64px;
    height: 4px;
    border-radius: 2px;
    background: #ff5a00;
}
.entry-content{
 font-size: 15px;
}
.image-content img{
    display: block;
    margin-top: 1%;
    width: 100%;
    height: 321px;
}
.featured-cause .cause-wrap {
    padding: 40px 24px;
}

.cause-wrap {
    margin-top: 50px;
}
.content-cause{
    /*width: calc(100%-280px);*/
        width: calc(100% - 280px);
}
.entry-title a{
    font-size: 18px;
    font-weight: 600;
    color: #282626;
}
figure img{
    width: 100%;
}
.posted-date a{
    padding-right: 14px;
    margin-right: 14px;
    display: block;
    position: relative;
    margin-top: 6px;
    font-size: 11px;
    font-weight: 500;
    line-height: 1;
    color: #262626;
}
.posted-date a::after{
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    width: 1px;
    height: 100%;
    background-color: #262626;
}
.posted-link a{
    display: block;
    position: relative;
    margin-top: 6px;
    font-size: 11px;
    font-weight: 500;
    line-height: 1;
    color: #262626;
}
.entry-content-build{
    margin-top: 28px;
    margin-bottom: 0;
    font-size: 14px;
    color: #1a1a1b;
}

.cause-wrap {
    padding: 40px 24px;
    background: #fcfcfc;
    border-radius: 4px;
    height: 300px;
    border:1px solid #d4d4d4ad;
}

.cause-wrap {
    margin-top: 50px;
}
.btn.gradient-bg {
    border-color: transparent;
    background: -moz-linear-gradient(180deg, rgba(255,90,0,1) 0%, rgba(255,54,0,1) 100%);
    background: -webkit-gradient(linear, left top, right top, color-stop(0%, rgba(255,90,0,1)), color-stop(100%, rgba(1255,54,0,1)));
    background: -webkit-linear-gradient(180deg, rgba(255,90,0,1) 0%, rgba(255,54,0,1) 100%);
    background: -o-linear-gradient(180deg, rgba(255,90,0,1) 0%, rgba(255,54,0,1) 100%);
    background: -ms-linear-gradient(180deg, rgba(255,90,0,1) 0%, rgba(255,54,0,1) 100%);
    background: linear-gradient(270deg, rgba(255,90,0,1) 0%, rgba(255,54,0,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5a00', endColorstr='#ff3600',GradientType=1 );
    color: #fff;
}
.entry-footer .btn {
    color: #fff;
    padding: 18px 40px;
    /*border: 2px solid #262626;*/
    border-radius: 24px;
    font-size: 14px;
    font-weight: 600;
    line-height: 1;
    /*color: #262626;*/
    outline: none;
}
.about-stat{
    text-align: center;
    color: #fff;
    padding: 96px 0;
    background: url("<?= base_url()?>assets/images/bg.jpg") no-repeat center;
    background-size: cover;
    margin-top: 2%;
    margin-bottom: 2%;
}
.about-stat img{
    width: 30%;
    border-radius: 30%;
    /*background-color: #fff;*/
}
.about-stat p{
    font-size: 16px;
}
.circural-progress-bar{
    margin: 20px 0;
    text-align: center;
}
.circural-progress-bar .circle{
    position: relative;
    width: 156px;
    height: 156px;
    margin: 0 auto;
}
.circular-progress-bar .circle strong {
    position: absolute;
    top: 50%;
    left: 0;
    margin-top: -24px;
    width: 100%;
    font-size: 48px;
    font-weight: 500;
    line-height: 1;
    color: #fff;
}
..circular-progress-bar .circle strong i {
    margin-top: 5px;
    font-size: 24px;
    font-style: normal;
}
</style>
<!-- <div class="name">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>About Us</h1>
            </div>
        </div>
    </div>
</div> -->
<div class="welcome " style="margin-top: 4%;">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 order-2 order-lg-1">
                <header>
                <h5 class="title-content">
                    Thing Group Convert
                </h5>
            </header>
            <div class="entry-content mt-5">
            <p class="jin"><?= $top['content']?></p>
            </div>
            <div class="entry-footer mt-5">
                <a class="btn gradient-bg mr-2" style="color: #fff">
                    Read More                    
                </a>
            </div>
            </div>
            <div class="col-12 col-lg-6 order-2 order-lg-1 image-content">
                <img src="<?= base_url() ?>assets/analytics.svg">
            </div>
        </div>
    </div>
</div>
<div class="about-stat">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3">
                <img src="<?= base_url()?>assets/images/about/map.svg">
                <p class="satu">Location</p>
                <span>Jl.Banyumas km 18 </span>
                </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <img src="<?= base_url()?>assets/images/about/servis.svg">
                <p>Cutomer Service</p>
                <span>081393549149</span>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                 <img src="<?= base_url()?>assets/images/about/email.svg">
                <p  class="satu">E-mail</p>
                <span>thing@thing.com</span>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <img src="<?= base_url()?>assets/images/about/skype.svg">
                <p  class="satu">Skype</p>
                <span>Thing~group</span>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
    <?php foreach ($center as $isi ):?>
        <div class="col-12 col-lg-6 boys">
            <div class="cause-wrap d-flex flex-wrap justify-content-between shadow-sm">
                <figure class="m-0 col-6">
                    <img src="<?= base_url()?>assets/images/about/<?= $isi['image']?>">
                </figure>
                <div class="content-cause">
                    <header class="entry-header d-flex flex-wrap align-items-center ">
                        <h3 class="entry-title w-100 m-0">
                            <a> <?= $isi['title']?></a>
                        </h3>
                        <div class="posted-date">
                            <a href="#">
                                Best practice
                            </a>
                        </div>
                        <div class="posted-link">
                            <a href="#">Fast Build</a>
                        </div>
                    </header>
                    <div class="entry-content-build">
                        <p class="m-0">
                            <?= $isi['content']?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
      <?php endforeach ?>
    </div>
</div>

    <!--/#services-->
    <!-- Content Kata-kata -->
<!--     <div class="isi_b text-align-center p-3">
        <div class="container">
            
    <div class="container">
    <div class="dua shadow-sm one">
        <div class="row">
            <div class="col-sm-3 isi">
                <img src="<?= base_url()?>assets/images/about/map.svg">
                <p class="satu">Location</p>
                <span>Jl.Banyumas km 18 </span>
            </div>
                <div class="col-sm-3">
                <img src="<?= base_url()?>assets/images/about/servis.svg">
                <p>Cutomer Service</p>
                <span>081393549149</span>
            </div>
                <div class="col-sm-3">
                <img src="<?= base_url()?>assets/images/about/skype.svg">
                <p  class="satu">Skype</p>
                <span>Thing~group</span>
            </div>
                <div class="col-sm-3">
                <img src="<?= base_url()?>assets/images/about/email.svg">
                <p  class="satu">E-mail</p>
                <span>thing@thing.com</span>
            </div>
        </div>

    </div>
    </div>


</div>
</div> -->
<style type="text/css">
    .isi_2{
        margin-left: 6%;
    }
</style>