<style type="text/css">
body{
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 11px;
}
@media(min-width:992px){
    .top img{
        width: 20%;
        margin-left: 0;
        height: 183px;
    }
    .top .img_2{
        width: 10%;
    }
    .top{
        width: 50%;
       margin-top: 79px;
        background-color: #d4d3d32e;
    }
    }
    .bos{
     -webkit-background-size: cover;
     -moz-background-size: cover;
     background-size: cover;
     color: #fff;
    font-family: "Montserrat", sans-serif;
    }
    .bos h2 small{
    /*margin-left: 12%;  */
    color: #fff;
    font-size: 41%;
    border-bottom: 1px solid #fff; 
    }
    .bos i{
        font-size: 215%;
    }
    .bos .tool{
    /*background-color: #d4d3d32e;*/
    padding: 10px;
    margin-top: 7%;
    /* bottom: 0px; */
    border-radius: 4px;
    }
    .bos p{
        font-size: 12px;
    }
    .bos{
        width: 80%;
        margin-bottom: 5%;
    }
    .video{
        width: 80%;
        max-height: 500px;
        margin-top: 5%;
        margin-bottom: 5%;
    }
    .content_1 p{
        font-size: 13px;
    }
    .tiga p{
        font-size: 13px;
    }
    .dua p{
        font-size: 13px;
    }
    .run{
        height: 400px;
        width: 800%;
        background-color: grey;
    }
    .home-page-icon-boxes{
        padding: 96px 0;
    }
    .icon-box{
        padding: 40px;
        border-radius: 24px;
        background-color: #ecf2f5;
        text-align: center;
        transition: all .35s;
    }
    .icon-box img{
        width: 30%;
    height: 132px;
    }
    .icon-box .entry-title {
    margin-top: 28px;
    font-size: 24px;
    font-weight: 600;
    color: #262626;
    }
    .icon-box .entry-content{
        margin-top: 24px;
    }
    .icon-box .entry-content p{
        margin: 0;
        font-size: 14px;
        color: #595858;
    }
    .home-page-welcome{
        position: relative;
        padding: 96px 0;
        background: url('<?= base_url() ?>assets/images/about/achievement-adult-aerial-1122403.jpg') no-repeat center;
        background-size: cover;
        z-index: 99;
    }
    .home-page-welcome::after{
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        background: -webkit-linear-gradient(left, rgba(21, 20, 20, 0), rgb(21, 20, 20));
        background: -o-linear-gradient(right, rgba(255,0,0,0), rgba(21, 20, 20));
        background: -moz-linear-gradient(right, rgba(255,0,0,0), rgba(21, 20, 20));
        background: linear-gradient(to left, rgba(255,0,0,0), rgb(234, 234, 234));
    }
    .home-page-welcome .entry-title{
        position: relative;
        padding-bottom: 24px;
        font-size: 36px;
        font-weight: 600;
        color: #000;
    }
    .home-page-welcome .entry-title::before{
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 64px;
        height: 4px;
        border-radius: 2px;
        background-color: #ff5a00;
    }
    .home-page-welcome p{
        font-size: 14px;
        line-height: 2;
        color: #000;
    }
    .home-page-welcome img{
        display: block;
        width: 100%;
        vertical-align: baseline;
    }
    /*home page 2*/
      .home-page-welcome_2{
        position: relative;
        padding: 96px 0;
        background-size: cover;
        z-index: 99;
    }
    .home-page-welcome_2 .entry-title{
        position: relative;
        padding-bottom: 24px;
        font-size: 36px;
        font-weight: 600;
        color: #000;
    }
    .home-page-welcome_2 p{
        color: #000;
        font-size: 14px;
    }
    .home-page-welcome_2 .entry-title::before{
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 64px;
        height: 4px;
        border-radius: 2px;
        background-color: #ff5a00;
    }
    .home-page-welcome_2 img{
        display: block;
        width: 100%;
        vertical-align: baseline;
    }
    /*home page 3*/
    .home-page-welcome_3{
        position: relative;
        padding: 96px 0;
        background: url('<?= base_url() ?>assets/images/about/adult-businessman-close-up-374820.jpg') no-repeat center;
        background-size: cover;
        z-index: 99;
    }
    .home-page-welcome_3::after{
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        background-color: rgba(21, 21, 21, 0.71);
    }
    .home-page-welcome_3 .entry-title{
        position: relative;
        padding-bottom: 24px;
        font-size: 36px;
        font-weight: 600;
        color: #fff;
    }
    .home-page-welcome_3 .entry-title::before{
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 64px;
        height: 4px;
        border-radius: 2px;
        background-color: #ff5a00;
    }
    .home-page-welcome_3 p{
        font-size: 14px;
        line-height: 2;
        color: #b7b7b7;
    }
    .home-page-welcome_3 img{
        display: block;
        width: 100%;
        vertical-align: baseline;
    }
    .carousel-inner .active{
         border-bottom: 3px solid #0fa2f1;
    color: #0fa2f1;
    }
video#bgvideo {
position: absolute;
width: 100%;
max-height: 100%;   
float: right;
z-index: -100;
top: 0;
bottom: 0;
margin-left: -2%;
/*-ms-transform: translateX(-50%) translateY(-50%);
-moz-transform: translateX(-50%) translateY(-50%);
-webkit-transform: translateX(-50%) translateY(-50%);
transform: translateX(-3%) translateY(-50%);*/
border-radius: 70%;
}
.hero{
    margin-top: 4%;
}
.footer-widgets {
    position: relative;
    padding: 96px 0 90px;
    font-weight: 500;
    color: #929191;
    background: url("<?= base_url() ?>assets/images/about/adult-businessman-close-up-374820.jpg") no-repeat center;
    background-size: cover;
    z-index: 99;
}
.footer-widgets h2 {
    margin-bottom: 40px;
    font-size: 18px;
    color: #fff;
}
.foot-about p {
    font-size: 14px;
    line-height: 2;
    color: #929191;
}
.footer-widgets ul {
    padding: 12px 0 0;
    margin: 0;
    list-style: none;
}
.foot-about ul li {
    margin-right: 28px;
}

.footer-widgets ul li {
    margin-bottom: 8px;
    font-size: 14px;
}
.foot-latest-news ul li {
    display: block;
    margin-bottom: 36px;
}

.footer-widgets ul li {
    margin-bottom: 8px;
    font-size: 14px;
}
.subscribe-form input[type="email"] {
    width: calc(100% - 70px);
    padding: 12px 16px;
    border: 0;
    font-size: 12px;
    line-height: 1;
    background: #fff;
    color: #131212;
    outline: none;
}
.subscribe-form input[type="submit"] {
    width: 68px;
    padding: 13px 0 14px;
    border: 0;
    font-size: 12px;
    line-height: 1;
    text-transform: uppercase;
    background: #ff5a00;
    color: #fff;
    cursor: pointer;
    outline: none;
}
.foot-contact ul li {
    display: flex;
    align-items: baseline;
    margin-bottom: 15px;
}
.foot-latest-news ul li h3 {
    font-size: 14px;
}
.foot-latest-news ul li .posted-date {
    font-size: 12px;
    color: #ff5a00;
}
.footer-widgets::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    width: 100%;
    height: 101%;
    background: rgba(22,22,22,.92);
}
.footer-bar {
    padding: 30px 0;
    font-size: 12px;
    color: #7b7b7b;
    background: #131212;
    text-align: center;
}
</style>
<div class="hero">
<!--     <div class="info">
        <span> thingshop121@gmail.com</span>
    </div>
       <div class="info_2">
        <label>www.thinkshop.com</span>
    </div> -->

<video playsinline autoplay muted loop id="bgvideo" width="100%" src="<?= base_url()?>assets/video/video.mp4" type="video/mp4">
</video>

<div class="col-12 col-lg-8 order-2 order-lg-1 bos">

  <h2><img src="<?= base_url();?>assets/images/logo.png" width="10%">~Thing Group  <small>improve your business with us,</small></h2>
  <div class="tool">
  <p><i class="glyphicon glyphicon-globe"></i> Connecting all user and partner</p>
  <p><i class="glyphicon glyphicon-phone"></i> Make easy your bu<b>siness</b></p>
  <p><i class="glyphicon glyphicon-cloud-upload"></i> Save your data</p>
  <p><i class="glyphicon glyphicon-tasks"></i> Build your Data Management</p>
</div>
    </div>


</div>
<div class="color">
    <div class="home-page-icon-boxes">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box">
                    <figure class="d-flex justify-content-center">
                        <img src="http://localhost/thing_group/assets/images/5.svg" style="display: block;">
                    </figure>
                    <header class="entry-header">
                        <h3 class="entry-title">Many partner</h3>
                    </header>
                    <div class="entry-content">
                        <p>from 1992 until now you have simplified your business.</p>
                    </div>
                </div>
            </div>
             <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box">
                    <figure class="d-flex justify-content-center">
                        <img src="<?= base_url();?>assets/images/database.svg" style="display: block;width: 57%;">
                    </figure>
                    <header class="entry-header">
                        <h3 class="entry-title">Build and Fast</h3>
                    </header>
                    <div class="entry-content">
                        <p>Work comfortably not speed, let go of mistakes and accidents.</p>
                    </div>
                </div>
            </div>
             <div class="col-12 col-md-6 col-lg-4 mt-4 mt-lg-0">
                <div class="icon-box">
                    <figure class="d-flex justify-content-center">
                        <img src="http://localhost/thing_group/assets/images/cloud.svg" style="display: block;">
                    </figure>
                    <header class="entry-header">
                        <h3 class="entry-title">Connection</h3>
                    </header>
                    <div class="entry-content">
                        <p>Alibaba,Microsoft,BNB,Facebook crate Deep company arrcive with us</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="home-page-welcome">
    <div class="container">
        <div class="row">
        <?php foreach ($isi as $dlm ):?>
            <?php if ($dlm['kode'] === "1") {?>
            <div class="col-12 col-lg-6 order-2 order-lg-1">
                <div class="welcome-content">
                    <header class="entry-header">
                        <h2 class="entry-title"><?= $dlm['title']?></h2>
                    </header>
                    <div class="entry-content mt-5">
                        <p><?= $dlm['content']?></p>
                    </div>
                    <div class="entry-footer mt-5"></div>
                </div>
            </div>
            <div class="col-12 col-lg-6 mt-4 order-1 order-lg-2">
                <img src="<?= base_url()?>assets/images/about/<?= $dlm['image']?>" >
            </div>
        </div>
    <?php } else {?>
    <?php } ?>
    <?php endforeach ?>
    </div>
</div>
<div class="home-page-welcome_2">
    <div class="container">
        <div class="row">
        <?php foreach ($isi as $dlm ):?>
            <?php if ($dlm['kode'] === "3") {?>
            <div class="col-12 col-lg-6 order-2 order-lg-1">
                <div class="welcome-content">
                    <header class="entry-header">
                        <h2 class="entry-title"><?= $dlm['title']?></h2>
                    </header>
                    <div class="entry-content mt-5">
                        <p><?= $dlm['content']?></p>
                    </div>
                    <div class="entry-footer mt-5"></div>
                </div>
            </div>
            <div class="col-12 col-lg-6 mt-4 order-1 order-lg-2">
                <img src="<?= base_url()?>assets/images/about/<?= $dlm['image']?>" >
            </div>
        </div>
    <?php } else {?>
    <?php } ?>
    <?php endforeach ?>
    </div>
</div>


<div class="home-page-welcome_3">
    <div class="container">
        <div class="row">
        <?php foreach ($isi as $dlm ):?>
            <?php if ($dlm['kode'] === "2") {?>
            <div class="col-12 col-lg-6 order-2 order-lg-1">
                <div class="welcome-content">
                    <header class="entry-header">
                        <h2 class="entry-title"><?= $dlm['title']?></h2>
                    </header>
                    <div class="entry-content mt-5">
                        <p><?= $dlm['content']?></p>
                    </div>
                    <div class="entry-footer mt-5"></div>
                </div>
            </div>
            <div class="col-12 col-lg-6 mt-4 order-1 order-lg-2">
                <img src="<?= base_url()?>assets/images/about/<?= $dlm['image']?>" >
            </div>
        </div>
    <?php } else {?>
    <?php } ?>
    <?php endforeach ?>
    </div>
</div>
<br>
<div class=" container " >
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <iframe height="400" width="100%" src="https://www.youtube.com/embed/0HnHUBeIBqU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen ><h2><img src="<?= base_url();?>assets/images/logo.png" width="10%"></iframe>
    </div>
    <div class="carousel-item">
     <iframe height="400" width="100%" src="https://www.youtube.com/embed/sxZlqFLIwSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="carousel-item">
    <iframe height="400" width="100%" src="https://www.youtube.com/embed/2EEk5G-rud0?rel=0&amp;vq=hd720 " frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</div>

</div>

<!--         <script type="text/javascript">

        $(document).ready(function(){
            tampil_data_barang();   //pemanggilan fungsi tampil barang.z

            $('#mydata').dataTable();

            //fungsi tampil barang
            function tampil_data_barang(){
                $.ajax({
                    type  : 'GET',
                    url   : '<?php echo base_url()?>index.php/Shop/data_toko',
                    async : false,
                    dataType : 'json',
                    success : function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++ ){
                            html += '<tr>'+
                                    '<td>'+(i+1)+'</td>'+
                                    '<td>'+data[i].Id+'</td>'+
                                    '<td>'+data[i].Join_since+'</td>'+
                                    '<td>'+data[i].total_sales+'</td>'+
                                    '<td>'+accounting.formatMoney(data[i].total_revenue)+'</td>'+
                                    '<td><a href="<?php echo base_url();?>index.php/Shop/Detail/'+data[i].No+'" class="btn tombol"><i class="fa fa-search-plus"></i> view</a></td>'+
                                    '</tr>';
                        }
                        $('#show_data').html(html);
                    }

                });
            }

        });
        Showing rows 0 - 0 (1 total, Query took 5.8416 seconds.)
</script> -->