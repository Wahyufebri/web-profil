<style type="text/css">

.popup-overlay{
  /*Hides pop-up when there is no "active" class*/
  visibility:hidden;
  margin-top: 7%;
  position:fixed;
  background:#ffffff;
  border:3px solid #4aa1ef;
  width:50%;
  height:auto;
  left:25%; 

}
.popup-overlay.active{
  /*displays pop-up when "active" class is present*/
  visibility:visible;
  text-align:center;
}

.popup-content {
  /*Hides pop-up content when there is no "active" class */
 visibility:hidden;
}

.popup-content img{
  width: 20%;
  height: 30%;
}
.popup-content h2{
  font-family: 'Margarine', cursive;
}
.popup-content.active {
  /*Shows pop-up content when "active" class is present */
  visibility:visible;
}
.hasil{
  display: none;
}
.dlm{
  background-color: pink;
  height: 100%;
}
.xpire{
  margin-top: 10%;
}
.popup-content a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

.popup-content button:hover, a:hover {
  opacity: 0.7;
  cursor: pointer;
}
.posisi {
  font-size: 30px;
  color: #828282;
  font-family:'PT Serif', serif;
}

/*

.row.heading h2 {
    color: #fff;
    font-size: 52.52px;
    line-height: 95px;
    font-weight: 400;
    text-align: center;
    margin: 0 0 40px;
    padding-bottom: 20px;
    text-transform: uppercase;
}
ul{
  margin:0;
  padding:0;
  list-style:none;
}
.heading.heading-icon {
    display: block;
}
.padding-lg {
  display: block;
  padding-top: 60px;
  padding-bottom: 60px;
}
.practice-area.padding-lg {
    padding-bottom: 55px;
    padding-top: 55px;
}
.practice-area .inner{ 
     border:1px solid #999999; 
   text-align:center; 
   margin-bottom:28px; 
   padding:40px 25px;
}
.our-webcoderskull .cnt-block:hover {
    box-shadow: 0px 0px 10px rgba(0,0,0,0.3);
    border: 0;
}
.practice-area .inner h3{ 
    color:#3c3c3c; 
  font-size:24px; 
  font-weight:500;
  font-family: 'Poppins', sans-serif;
  padding: 10px 0;
}
.practice-area .inner p{ 
    font-size:14px; 
  line-height:22px; 
  font-weight:400;
}
.practice-area .inner img{
  display:inline-block;
}
*/
/*
.our-webcoderskull{
  background: url("http://www.webcoderskull.com/img/right-sider-banner.png") no-repeat center top / cover;
  
}
.our-webcoderskull .cnt-block{ 
   float:left; 
   width:100%; 
   background:#fff; 
   padding:30px 20px; 
   text-align:center; 
   border:2px solid #d5d5d5;
   margin: 0 0 28px;
}
.our-webcoderskull .cnt-block figure{
   width:148px; 
   height:148px; 
   border-radius:100%; 
   display:inline-block;
   margin-bottom: 15px;
}
.our-webcoderskull .cnt-block img{ 
   width:148px; 
   height:148px; 
   border-radius:100%; 
}
.our-webcoderskull .cnt-block h3{ 
   color:#2a2a2a; 
   font-size:20px; 
   font-weight:500; 
   padding:6px 0;
   text-transform:uppercase;
}
.our-webcoderskull .cnt-block h3 a{
  text-decoration:none;
  color:#2a2a2a;
}
.our-webcoderskull .cnt-block h3 a:hover{
  color:#337ab7;
}
.our-webcoderskull .cnt-block p{ 
   color:#2a2a2a; 
   font-size:13px; 
   line-height:20px; 
   font-weight:400;
}
.our-webcoderskull .cnt-block .follow-us{
  margin:20px 0 0;
}
.our-webcoderskull .cnt-block .follow-us li{ 
    display:inline-block; 
  width:auto; 
  margin:0 5px;
}
.our-webcoderskull .cnt-block .follow-us li .fa{ 
   font-size:24px; 
   color:#767676;
}
.our-webcoderskull .cnt-block .follow-us li .fa:hover{ 
   color:#025a8e;
}
*/
.pt-100{
    padding-top:100px;
}
.pb-100{
    padding-bottom:100px;
}
.section-title {
    margin-top: 20%;
}
.section-title p {
  color: #777;
  font-size: 13px;
}
.section-title h4 {
  text-transform: capitalize;
  font-size: 28px;
  position: relative;
  padding-bottom: 20px;
  margin-bottom: 20px;
  font-weight: 600;
}
.section-title h4:before {
  position: absolute;
  content: "";
  width: 60px;
  height: 2px;
  background-color: #ff3636;
  bottom: 0;
  left: 50%;
  margin-left: -30px;
}
.section-title h4:after {
  position: absolute;
  background-color: #ff3636;
  content: "";
  width: 10px;
  height: 10px;
  bottom: -4px;
  left: 50%;
  margin-left: -5px;
  border-radius: 50%;
}
body {
    margin:0;
    padding:0;
    font-family:sans-serif;
    background:#fbfbfb;
}
.card {
    position:relative;
    float: right;
    /*top:2%;*/
    /*transform:translate(-20%,-20%);*/
    width:250px;
    min-height:400px;
    background:#fff;
    /*box-shadow:0 20px 50px rgba(0,0,0,.1);*/
    border-radius:10px;
    transition:0.5s;
}
.card:hover {
    box-shadow:0 30px 70px rgba(0,0,0,.2);
}
.card .box {
    position:absolute;
    top:50%;
    left:0;
    transform:translateY(-50%);
    text-align:center;
    padding:20px;
    box-sizing:border-box;
    width:100%;
}
.card .box .img {
    width:120px;
    height:120px;
    margin:0 auto;
    border-radius:50%;
    overflow:hidden;
    cursor: pointer;
}
.card .box .img img {
    width:100%;
    height:100%;
}
.card .box h2 {
    font-size:20px;
    color:#262626;
    margin:20px auto;
}
.card .box h2 span {
    font-size:14px;
    background:#2bace2;
    color:#fff;
    display:inline-block;
    padding:4px 10px;
    border-radius:15px;
}
.card .box p {
    color:#262626;
}
.card .box span {
    display:inline-flex;
}
.card .box ul {
    margin:0;
    padding:0;
}
.card .box ul li {
    list-style:none;
    float:left;
}
.card .box ul li a {
    display:block;
    color:#aaa;
    margin:0 10px;
    font-size:20px;
    transition:0.5s;
    text-align:center;
}
.card .box ul li:hover a {
    color:#e91e63;
    transform:rotateY(360deg);
}
</style>
        <div class=" mx-auto text-center ">
          <div class="section-title">
            <h4>Design Team</h4>
            <p>Many team in the world build or create project for great income</p>
          </div>
        </div>
        <div class="container">
        <div class="row">
    <?php foreach ($tim as $design) :?>
      <?php if ($design['devisi'] == "Design"){?>
          <div class="card m-auto">
         <div class="box">
        <div class="img">
            <img src="<?= base_url()?>assets/images/tim/member/<?= $design['gambar'] ?>"  onclick="tampil('<?= $design['id']?>')">
        </div>
        <h2><?= $design['nama'] ?><br><span><?= $design['posisi'] ?></span></h2>
        <p><?= substr($design['biodata'],0,50)?> </p>
        <span>
            <ul>
                <li><a href="https://www.facebook.com/<?= $design['sosmed']?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/<?= $design['sosmed_2']?>" ><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="https://plus.google.com/<?= $design['sosmed_4']?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/<?= $design['sosmed_3']?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
             </span>
          </div>
      </div>
     <?php } else { ?>

     <?php } ?>
 <?php endforeach ?>
</div>
</div>
  <div class=" mx-auto text-center ">
  <div class="section-title">
    <h4>Developer Team</h4>
     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
      </div>
        </div>
        <div class="container">
        <div class="row">
        <?php foreach ($tim as $develop) :?>
        <?php if ($develop['devisi'] == "progam"){?>
          <div class="card m-auto">
         <div class="box">
        <div class="img">
            <img src="<?= base_url()?>assets/images/tim/member/<?= $develop['gambar'] ?>"  onclick="tampil('<?= $develop['id']?>')">
        </div>
        <h2><?= $develop['nama'] ?><br><span><?= $develop['posisi'] ?></span></h2>
        <p><?= substr($develop['biodata'],0,50)?> </p>
        <span>
            <ul>
                <li><a href="https://www.facebook.com/<?= $develop['sosmed']?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="https://twitter.com/<?= $develop['sosmed_2']?>" ><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="https://plus.google.com/<?= $develop['sosmed_4']?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/<?= $develop['sosmed_3']?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
             </span>
          </div>
      </div>
     <?php } else { ?>

     <?php } ?>
 <?php endforeach ?>
</div>
</div>
         

<!-- Developer -->
<!--     <div class="container jadi">
    <div class="sub">
       <div class="judul">
        <h3>Developer Team</h3>
      </div>
    </div>
</div> -->

</div>
<div class="popup-overlay  rounded-lg shadow-lg ">
  <!--Creates the popup content-->
   <div class="popup-content " id="convert">
    
      </div>
</div>
