<style type="text/css">
    .project{
    padding: 30px;
/*    background-color: #fafafa;*/

    }
    .atas{
        margin-top: 2%;
    }
    .portofolio-wrap{
        padding:96px 0px;
    }
    .portofolio-wrap .portofolio-item.visible {
    display: block;
    }

    .portofolio-wrap .portofolio-item {
    display: none;
    margin-bottom: 30px;
    }
    .portofolio-cont{
        margin-bottom:  20px;
    }
    .portofolio-cont img{
        width: 100%;
    }
    .entry-title{
        margin-top: 12px;
        font-size: 18px;
        font-weight: 800;
    }
    a{
        color: #262626;
    }
    .portofolio-cont h4{
        margin-top: 10px;
        font-size: 14px;
        font-weight: 500;
        color: #ff5a00;
    }
</style>
<div class="atas"></div>

        <div class="portofolio-wrap">
            <div class="container">
                <div class="row portofolio-container">
                <?php foreach($content as $key ):?>
                    <div class="col-12 col-md-6 col-lg-4 potofolio-item visible">
                        <div class="portofolio-cont">
                            <a href="">
                                <img src="<?= base_url() ;?>assets/images/tim/member/<?= $key['gambar'] ?>">
                            </a>
                            <h3 class="entry-title">
                                <a href="#"><?= $key['nama']?></a>            
                            </h3>
                            <h4>2018 Couse </h4>
                        </div>
                    </div>
            <?php endforeach ?>
        </div>
            </div>
        </div>
        <?= $halaman ?>
