-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 13, 2019 at 08:00 PM
-- Server version: 10.1.38-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `content` varchar(200) NOT NULL,
  `bagian` text NOT NULL,
  `kode` enum('1','2','3','') NOT NULL,
  `sub` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `image`, `content`, `bagian`, `kode`, `sub`) VALUES
(4, 'About Me', '1486146609-propertyagent005_79444.svg', 'Thing is company for make or build application or software for the the business and thing have a ecommerce for baby fashion etc', 'top', '', 'about'),
(5, 'Web Development', 'contoh.jpg', 'Web is platform from www and web is very easy for use no make system weight no to have create have large memory because web is for all platfrom andrid ios windows linux mac_os and other', 'center', '', 'about'),
(6, 'Desktop Development', 'desktop.svg', 'In years 2019 Desktop application Development is tiny and if you look in many mall store and company need desktop application for manage our buciness and in here we come for solve and help you about ,', 'center', '2', 'about'),
(7, 'Android Development', 'chat.svg', 'Android is best platform in the world this years because many mony oll in android example application is Ali_pay,Tokopedia,Rakutan and other application we create and we develop', 'center', '', 'about'),
(8, 'History', 'team.svg\r\n', 'we set up a thing company from 2017 we struggled from scratch from a broken laptop that was no longer reasonable to use, we ran from 2 people on the team and now we have 2000 workers', 'center', '1', 'home'),
(9, 'Enhancement', 'rate.png', 'we have overcome various obstacles from the discharge of employees and we believe we will rise and make a breakthrough. And now we prove that there are 3000 fellow busnis.', 'center', '2', 'home'),
(10, 'Response', 'cr.png', 'We have established 3 years and we prioritize our money to prioritize comment comments from our customers to continue to improve and improve to date', 'center', '1', 'home');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pesan` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `id_admin`, `id_user`, `pesan`, `file`, `time`) VALUES
(40, 0, 56, 'hallo jon', '', '2019-04-03 14:01:07'),
(41, 0, 56, 'piye kabarmu', '', '2019-04-03 14:01:16'),
(42, 0, 56, 'apik ?', '', '2019-04-03 14:01:21'),
(43, 52, 56, 'Maaf anda akan saya tuntut', '', '2019-04-04 03:19:55'),
(44, 0, 54, 'bro', '', '2019-04-04 09:48:48'),
(45, 52, 54, 'ya ok', '', '2019-04-04 09:49:31'),
(46, 0, 54, 'halo', '', '2019-04-08 09:26:13');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `project` varchar(200) NOT NULL,
  `dateline` date NOT NULL,
  `star_date` date NOT NULL,
  `gambaran` varchar(250) NOT NULL,
  `komplain` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` varchar(200) NOT NULL,
  `bagian` varchar(20) NOT NULL,
  `kode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `isi`
--

CREATE TABLE `isi` (
  `no` int(11) NOT NULL,
  `isi` varchar(200) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `konten` enum('utama','sub','small','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `nama` varchar(160) NOT NULL,
  `gambar` varchar(160) NOT NULL,
  `desc` varchar(160) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `nama`, `gambar`, `desc`) VALUES
(6, 'Game develop', 'image.jpeg', 'Best machinewe make this software with assembly lenguage and we cretae api project and you can help another project example to create image new'),
(7, 'cashier application', 'image2.jpg', 'for bing store in 12 country we create with python scurity and we build this application need 12 hours 2 team build this project'),
(8, 'Video editor application ', 'imgage3.jpeg', 'For adobe development.Adobe creating many application ad we thing group build one appplication is adobe DFG for manag user activity'),
(9, 'Image sorters', 'image4.jpg', 'Image sorter for JDK film,Image sorting with tansor flow.js this software we make with AI first level to detect image and sorting '),
(10, 'blog', 'animator-hd1.jpg', 'Manifacture.Blog for Jhon alina boys,quistion from jhon to thing group what unique web can you build and we answer no we are no create unique web but we create '),
(11, 'Crack and safe company data', 'login-background-images-for-website-11.jpg', 'we create many scure system wirh python and matlab,Inthe world no system is save');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `posisi` varchar(50) NOT NULL,
  `devisi` varchar(100) NOT NULL,
  `biodata` varchar(100) NOT NULL,
  `sosmed` varchar(40) NOT NULL,
  `sosmed_2` varchar(40) NOT NULL,
  `sosmed_3` varchar(40) NOT NULL,
  `sosmed_4` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `nama`, `gambar`, `posisi`, `devisi`, `biodata`, `sosmed`, `sosmed_2`, `sosmed_3`, `sosmed_4`) VALUES
(2, 'Rice maher', 'boy2.jpeg', 'Design', 'Design', '<p>Rojak bes programmer for anny platfrom ,an him graduation from harvard university</p>\r\n', 'Rojak_008', 'Rojak_008', 'Rojak_008', 'Rojak_008'),
(4, 'Deni Zoung', '5.jpg', 'Senior developer', 'progam', 'Best programmer from Harvard university and him have a book for young people start the business', 'Deni_Zoungep', 'Deni_Zoungep', 'Deni_Zoungep', 'Deni_Zoungep'),
(5, 'Zoung zing', '3.jpg', 'Design', 'Design', 'Create assets for any project software and him have a small company design', 'Zoungaing', 'Zoungaing2', 'Zoungaing1', 'Zoungaing12'),
(6, 'ranoik', '8.jpg', 'Junior developer', 'progam', 'Melina is new programmer for Alizile university', 'Melina23', 'Melina23', 'Melina23', 'Melina23'),
(7, 'Ben zik', 'boy_6.jpg', 'Senior developer', 'progam', 'senior developer android and web.and him is manager web development', 'Ben_zik', 'Ben_zik', 'Ben_zik', 'Ben_zik'),
(8, 'Rands_dik', '6.jpeg', 'Video editor', 'Design', 'he was the originator of the establishment of the agro thing and he was our youngest programmer', 'Rands_dik', 'Rands_dik', 'Rands_dik', 'Rands_dik'),
(9, 'erick', '4.jpg', 'Design', 'Design', 'his is a professional design and he has many skills', 'Ling_nim', 'Ling_nim', 'Ling_nim', 'Ling_nim'),
(10, 'Bela luno', '7.jpg', 'Junior developer', 'progam', 'senior design in thing_group and his have skill for any project', 'Bela_luno', 'Bela_luno', 'Bela_luno', 'Bela_luno');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `level` enum('admin','user','atasan''','block_user','block_admin') NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `gender` enum('Male','Female','','') NOT NULL,
  `alamat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `no_hp`, `email`, `level`, `gambar`, `gender`, `alamat`) VALUES
(52, 'lani', '$2y$10$4DDekusBGfl0iBsNYCEtuurbm8gVlRVz/zw8AqpoUIBNkpSncAtY2', '', '', '', 'admin', '', 'Male', ''),
(54, 'ipul', '$2y$10$ND/ULMYCiJpLMfAQbuQc5.DgY8y3AB0aKLjKRpxnIp0OdXRzEqiiy', 'ipulll', '09875656', 'joni@gmail.com', 'user', '70f0cfc7-67a5-4a5b-92e4-dbab4d719ff6.png', 'Female', 'bali jakarta'),
(56, 'jono', '$2y$10$Sb/zbT3h/qTuX5lqFGVCEOlCgSZN0XSmjoqw2Xd8TbqfGMehBNZ/K', '', '', 'jono@gmail.com', 'user', '', 'Male', ''),
(57, 'roni', '$2y$10$2x.gdqwWcZfytSoCwdfD0uk8z6vIxhr7IK9XWDP99x69Ze5iyx2g6', '', '', 'roniangga04@gmail.com', 'user', '', 'Male', ''),
(58, 'asdd', '$2y$10$c.Oe4su6Y0KyvMdCOLMVY.ZSD7vCxgAUS1g3PySSS8HenXlbnrgoe', '', '', 'asdd', 'user', '', 'Male', ''),
(59, 'asdd', '$2y$10$Y0BlJ62xegwc1cg7ryvoke3b.CSM4TQATIiKfcpVCEILWlKMfEQsm', '', '', 'asdd', 'user', '', 'Male', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `isi`
--
ALTER TABLE `isi`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `isi`
--
ALTER TABLE `isi`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
